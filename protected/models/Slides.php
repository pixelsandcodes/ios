<?php

/**
 * This is the model class for table "slides".
 *
 * The followings are the available columns in table 'slides':
 * @property integer $ID
 * @property integer $ID_item
 * @property integer $ID_image
 */
class Slides extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Slides the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'slides';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID, slide_order', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, ID_item, file, video_poster, name, type, slide_order, fullscreen', 'safe', 'on'=>'search'),
		);
	}
        	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'item'=>array(self::BELONGS_TO, 'Items', 'ID_item'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'ID_item' => 'Id Item',
			'fullscreen' => 'Fullscreen Image',
                        'slide_order' => 'Slide Order',
                        'type' => 'Type',
                        'video_poster' => 'Video Poster',
                        'name' => 'Name',
                        'file' => 'File'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('ID_item',$this->ID_item);
		$criteria->compare('fullscreen',$this->fullscreene);
                $criteria->compare('image',$this->image);
                $criteria->compare('order',$this->order);
                $criteria->compare('type',$this->type);
                $criteria->compare('name',$this->name);
                $criteria->compare('video_poster',$this->video_poster);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}