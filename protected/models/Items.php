<?php

/**
 * This is the model class for table "items".
 *
 * The followings are the available columns in table 'items':
 * @property integer $ID
 * @property integer $ID_category
 * @property string $thumbnail
 * @property string $name
 * @property string $summary
 * @property string $link
 */
class Items extends CActiveRecord
{
    
        public function primaryKey()
        {
          return array('id');
        }
	/**
	 * Returns the static model of the specified AR class.
	 * @return Items the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID_category, thumbnail, name', 'required'),
			array('thumbnail, name, link', 'length', 'max'=>100),
			array('url', 'length', 'max'=>1000),
			array('summary, text', 'length', 'max'=>10000),
            array('thumbnail', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update', 'maxSize'=>1024 * 1024 * 10, 'tooLarge'=>'File has to be smaller than 30MB'),
            array('published', 'boolean'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, ID_category, thumbnail, name, url, published, summary, link, text, date_created', 'safe', 'on'=>'search'),
		);
	}
        
        public function scopes() {
            return array(
                'order' => array( 'order' => Order::model()->findByPk(1)->order ) 
            );
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'categoryName'=>array(self::BELONGS_TO, 'Categories', 'ID_category'),
                    'slide'=>array(self::HAS_MANY, 'Slides', 'ID_item', 'order'=>'slide_order ASC', ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'ID_category' => 'Id Category',
			'thumbnail' => 'Thumbnail',
			'name' => 'Name',
			'url' => 'URL Slug',
			'summary' => 'Summary',
			'link' => 'Link',
            'text' => 'Text',
            'published' => 'Published',
            'date_created'=>'Date'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('ID_category',$this->ID_category);
		$criteria->compare('thumbnail',$this->thumbnail,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('summary',$this->summary,true);
		$criteria->compare('link',$this->link,true);
        $criteria->compare('text',$this->text,true);
        $criteria->compare('published',$this->published,true);
        $criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}