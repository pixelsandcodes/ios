<?php
/**
 * Rest Controller for bacbone.js models
 *
 * @author vladimir
 */
class ItemsController extends ERestController {

	public function filters() {
        $restFilters = array('restAccessRules+ restList restCreate restUpdate restDelete');
        if(method_exists($this, '_filters'))
                return CMap::mergeArray($restFilters, $this->_filters());
        else
                return $restFilters;
    }
 
    public function accessRules() {
        $restAccessRules = array(
            array('allow',
                  'actions'=>array('restCreate', 'restList', 'restUpdate', 'restDelete', 'error'),
                  'users'=>array('*'),
            ));

        if(method_exists($this, '_accessRules'))
            return CMap::mergeArray($restAccessRules, $this->_accessRules());
        else
            return $restAccessRules;
    }

}