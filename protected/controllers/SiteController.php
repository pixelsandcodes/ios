<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */

	private function getItems() {
		$criteria = new CDbCriteria;
		$criteria->condition = 'published = 1';
		if(Order::model()->findByPk(2)->order) {
			$criteria->select = '*, rand() as rand';
			$criteria->order = 'rand';

		};

		$model = Items::model()->order()->findAll($criteria);
		$items = array();
		foreach ($model as $k => $item) {
			$hidden = 0;
			$cats = split(';', $item->ID_category);
			foreach ($cats as $c) {
				$cm = Categories::model()->findByPk($c);
				if($cm->published == 0)
					$hidden++;
			}
			if($hidden !== count($cats))
				array_push($items, $item);
		}
		return $items;
	}

	public function actionIndex()
	{		
		
		$this->render('index', array('model'=>$this->getItems()));
	}
        
        public function actionBackbone()
	{
		
		$this->render('js');
	}
        
        public function actionItems($id)
	{
                $model = Items::model()->findAll('ID_category = "'.$id.'"');
		
		$this->renderPartial('items', array('id'=>$model));
	}
        
        public function actionDetailedItem($id)
	{
                $model = Items::model()->findByPk($id);
		
		$this->renderPartial('item', array('item'=>$model));
	}
        
        public function actionFullscreenItem($id)
	{
                $model = Items::model()->findByPk($id);
		
		$this->renderPartial('fullscreen', array('item'=>$model));
	}
        
        public function actionVideos($id)
	{
                $slide = Slides::model()->with('item')->findByPk($id);

                $types = array('video/ogg', 'video/webm', 'video/mp4');
			    $videos = array();
			    $dir = Yii::getPathOfAlias('webroot')."/images/projects/project_".$slide->item->url."/slide_".$slide->slide_order."_".$slide->ID;
			    $url = Yii::app()->request->getBaseUrl(true)."/images/projects/project_".$slide->item->url."/slide_".$slide->slide_order."_".$slide->ID;
			    if ($handle = opendir($dir)) {

			        while (false !== ($entry = readdir($handle))) {
			            $type = CFileHelper::getMimeTypeByExtension($entry);
			            if(in_array($type, $types)) {
			                array_push($videos, array('src'=>$url.'/'.$entry, 'type'=>$type));
			            }
			        }

			        closedir($handle);
			    }
					
		$this->renderPartial('video', array('data'=>$videos));
	}

	public function actionSearch()
	{	
		$res =array();

            if (isset($_POST['term'])) {                
                $criteria = new CDbCriteria;
            	$criteria->select = 'ID, name, text';
            	$criteria->condition = 'name LIKE :term OR text LIKE :term AND published = 1';
            	$criteria->params = array(':term'=>"%".$_POST['term']."%");
            	$items = Items::model()->findAll($criteria);
            	foreach ($items as $item) {
            		array_push($res, array('label'=>$item->name, 'value'=>$item->name." ".$item->text, 'box'=>$item->ID));
            	}
            }

            $this->renderPartial('video', array('data'=>$res));
	}
        
        public function actionCategories()
	{
                $model =Categories::model()->findAll(array('order' =>'cat_order ASC', 'condition'=>'published = 1'));
		
		$this->render('categories', array('model'=>$model));
	}  
        
        public function actionAbout()
	{
		$this->render('about');
	} 

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}