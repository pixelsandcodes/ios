<?php
/**
 * Rest Controller for bacbone.js models
 *
 * @author vladimir
 */
class SlidesController extends ERestController {
    
    public function filters() {
        $restFilters = array('restAccessRules+ restCreate restUpdate restDelete');
        if(method_exists($this, '_filters'))
                return CMap::mergeArray($restFilters, $this->_filters());
        else
                return $restFilters;
    } 
 
    public function accessRules() {
        $restAccessRules = array(
            array('allow',
                  'actions'=>array('restCreate', 'restUpdate', 'restDelete', 'error'),
                  'users'=>array('*'),
            ));

        if(method_exists($this, '_accessRules'))
            return CMap::mergeArray($restAccessRules, $this->_accessRules());
        else
            return $restAccessRules;
    }
    
    public function doRestList() { 
        $this->outputJson( 
            $this->getModel()->with($this->nestedRelations)->filter($this->restFilter)->limit($this->restLimit)->offset($this->restOffset)->findAll()
        ); 
    }

    public function actionItem($id) { 
        $this->outputJson( 
            Slides::model()->findAll( array('condition'=>'id_item = '. $id) )
        ); 
    }

     public function doRestDelete($id) {
        $model = parent::loadOneModel($id);
        $item = Items::model()->findByPk($model->ID_item);
        $dir = Yii::getPathOfAlias('webroot')."/images/projects/project_".$item->url."/slide_".$model->slide_order."_".$model->ID;
        $this->deleteDir($dir);
        return parent::doRestDelete($id); 
    }

    function deleteDir($directory) {
        if (is_dir($directory))
            $dir = opendir($directory);
        else
            return false;
          
        while($file = readdir($dir)) {
          
            if ($file != "." && $file != "..")
            {

                if (!is_dir($directory."/".$file))
                    unlink($directory."/".$file);
                else
                    $this->deleteDir($directory.'/'.$file);   
            }
        }
        closedir($dir);
        rmdir($directory);
        return true;
    }
    
}

?>
