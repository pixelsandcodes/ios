<?php

class DefaultController extends ERestController
{
    
        public $layout='//layouts/admin';

        public $output = '';

        public $credentials = array('username'=>'admin@restuser', 'password'=>'admin@Access');
        
        public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update','view', 'index'),
				'users'=>UserModule::getAdmins(),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
	public function actionIndex()
	{
            $model = Items::model()->order()->findAll();
		$this->render('index', array('items'=>$model, 'output'=>$this->output));
	}

    public function actionAngular()
    {
            $model = Items::model()->order()->findAll();
        $this->renderPartial('angular', array('items'=>$model ));
    }

    public function actionJson()
    {
        $model = Items::model()->order()->findAll();
        print_r($model);
    }

    public function actionItems()
    {
            $model = Items::model()->order()->findAll();
        $this->renderPartial('items', array('items'=>$model));
    }
        
        public function actionSort($id)
	{
            $orderModel = Order::model()->findByPk(1);
            $randomModel = Order::model()->findByPk(2);
            $order;
            switch ($id) {
                case 0:
                    $order = "id";
                    break;
                case 1:
                    $order = "name";
                    break;
                case 2:
                    $order = "date_created DESC";
                    break;
                 case 3:
                    $order = $orderModel->order;
                    break;
            }
            
            $orderModel->order = $order;
            if($orderModel->save()) {
                
                if($id == 3) {
                    $model = Items::model()->order()->findAll(array('select'=>'*, rand() as rand', 'order'=>'rand'));
                    $randomModel->order = 1;
                }
                    
                else {
                    $model = Items::model()->order()->findAll();
                    $randomModel->order = 0;
                }
                if($randomModel->save())    
                    $this->renderPartial('items', array('items'=>$model)); 
            }            
        
        }
        
        public function actionView($id)
	{
            $model = Items::model()->findByPk($id);
		$this->render('viewitem', array('item'=>$model));
	}
        
        public function actionCategories()
	{
            $model = Categories::model()->findAll(array('order' =>'cat_order ASC'));
		$this->render('categories', array('model'=>$model));
	}
        
        public function actionViewCategory($id)
	{
            $model = Categories::model()->findByPk($id);
		$this->render('viewcat', array('model'=>$model));
	}
        
        public function actionUploadForm($id) {

                $this->render('uploadform');
        }
        
        public function actionUploadFile() {
            if(isset($_POST["name"]) && isset($_FILES["file"])) {
                $file = $_FILES["file"];
                $path = Yii::getPathOfAlias('webroot').'/images/temp/';
                $path_info = pathinfo($file['name']);
                $name = $this->friendlyUrl($_POST["name"]."_".$file["tmp_name"]."_".$path_info['filename']).".".$path_info['extension'];
                if(move_uploaded_file($file['tmp_name'], $path.$name)){
                    $this->renderJson(array('success'=> true, 'filename' => $name));
                } else {
                    $this->renderJson(array('success'=> false));
                }
               
            } else
                throw new CHttpException(404,'This page does not exist.');
        }

    public function actionUpdate($id) {
        $items = Items::model()->findByPk($id);
        $slides = Slides::model()->findAll(array("condition"=>"ID_item =  $id", "order"=>"slide_order ASC"));
        
        if(isset($_POST['slide']))
            return $this->saveSlide($_POST['slide'], $items);

        if(isset($_POST['model'])) {
            $model = $_POST['model'];
            $changedImage = false;
            $renamedFolder = false;
            if($model['name'] != $items->name)
                $renamedFolder = Yii::getPathOfAlias('webroot')."/images/projects/project_".$items->url;

            if($model['thumbnail'] != $items->thumbnail) 
                $changedImage = $items->thumbnail;

            $items->attributes = $model;
            $items->url = $this->friendlyUrl($model['name']);
            $items->thumbnail = $model['thumbnail'];

            if($items->save()) {
                if($changedImage)
                    $this->saveItemImage($items, $changedImage);
                if($renamedFolder)
                    $this->renameItemFolder($items, $renamedFolder);

                $this->renderJson(array('success'=>true, 'model'=>$items, 'rename' => $renamedFolder, 'new_image' => $changedImage));
            }
                
            else
                $this->renderJson(array('success'=>true, 'errors'=>$items->getErrors()));

        } else {
            $this->render('form', array("items"=>$items, "slides"=>$slides, 'credentials' => $this->credentials));
        }
    }

    public function actionCreate()
    { 

        if(isset($_POST['slide'])) {
            $item = Items::model()->findByPk($_POST['slide']['ID_item']);
            return $this->createSlide($_POST['slide'], $item);
        }
        
        $items = new Items();               

        if(isset($_POST['model'])) {
            $model = $_POST['model'];
            $items->attributes = $model;
            $items->ID = Yii::app()->db->createCommand()->select('max(ID) as max')->from('items')->queryScalar() + 1;
            $items->url = $this->friendlyUrl($model['name']);
            $items->thumbnail = $model['thumbnail'];
            $items->ID_category = 1;
            if($items->save()) {
                if(mkdir(Yii::getPathOfAlias('webroot')."/images/projects/project_".$items->url, 0777, true))
                    $this->saveItemImage($items);

                $this->renderJson(array('success'=>true, 'model'=>$items));
            }
                
            else
                $this->renderJson(array('success'=>false, 'errors'=>$items->getErrors()));
        }
        else
            $this->render('form', array("items"=>$items, "slides"=>array(), 'credentials' => $this->credentials));

    }

    private function saveSlide($model, $item) { 
        
        if(isset($model['ID']))
            $slide = Slides::model()->findByPk($model['ID']);
        else 
            return $this->createSlide($model, $item);

        $changedOrder = false;
        $changedImage = false;
        $changedFullscreenImage = false;
        if($slide->slide_order != $model['slide_order'])
            $changedOrder = $slide->slide_order;
        if($slide->file != $model['file'])
            $changedImage = $slide->file;
        if($slide->fullscreen != $model['fullscreen'])
            $changedFullscreenImage = $slide->fullscreen;

        foreach ($model as $key => $value) {
            $slide->$key = $value;
        }

        if($slide->save()){
            if($changedOrder)
                $this->renameSlideFolder($slide, $item, $changedOrder);
            if($changedImage)
                $this->saveSlideImage($slide, $item, $changedImage);
            if($changedFullscreenImage)
                $this->saveSlideImage($slide, $item, $changedImage, true);

            $this->renderJson(array('success'=>true, 'slide' => $slide, 'item' => $item));
        } else
            $this->renderJson(array('success'=>false ,'errors' => $slide->getErrors()));
    }

    private function saveItemImage($model, $oldImage = false) {
        $itemDir = Yii::getPathOfAlias('webroot')."/images/projects/project_".$model->url."/";
        if($oldImage) {
            if(file_exists($itemDir.$oldImage))
                unlink($itemDir.$oldImage);
        }
        $tempImage = Yii::getPathOfAlias('webroot')."/images/temp/".$model->thumbnail;
        return $this->moveTempImage($tempImage, $itemDir.$model->thumbnail);
    }

    private function saveSlideImage($model, $item,  $oldImage = false, $isFullscreen = false) {
        $itemDir = Yii::getPathOfAlias('webroot')."/images/projects/project_".$item->url."/slide_".$model->slide_order."_".$model->ID."/";
        if($isFullscreen) {
            $itemDir .= "fullscreen/";
            if(!is_dir($itemDir))
                if(!mkdir($itemDir, 0777, true))
                    throw new CHttpException(500, "Cannot create folder : $itemDir");                
        }            

        if($oldImage) {
            if(file_exists($itemDir.$oldImage))
                unlink($itemDir.$oldImage);
        }

        $attr = $isFullscreen ? 'fullscreen' : 'file';
        $tempImage = Yii::getPathOfAlias('webroot')."/images/temp/".$model->$attr;
        
        return $this->moveTempImage($tempImage, $itemDir.$model->$attr);
    }

    private function moveTempImage($tempImage, $newLocation) {
        if(file_exists($tempImage))
            if(copy($tempImage, $newLocation))
                return unlink($tempImage);
        else {
            throw new CHttpException(500, "Failed To Copy Temp image: $tempImage to project Folder: $newLocation");
            $Yii::app()->end();
        }
    }

    private function renameSlideFolder($model, $item, $order, $newModel = false) {
        $project_dir = Yii::getPathOfAlias('webroot')."/images/projects/project_".$item->url;
        $newDir = $project_dir."/slide_".$model->slide_order."_".$model->ID;
        $oldDir = $project_dir."/slide_".$order."_".$model->ID;
        if($newModel) {
            if(!mkdir($newDir, 0777, true))
                throw new CHttpException(500, "Failed To create new Slide directory: $newDir");
        } else {
            if(!rename($oldDir, $newDir))
                throw new CHttpException(500, "Error while renaming folder: $oldDir");
        }
    }

    private function renameItemFolder($model, $folderToBeRenamed) {
        $newFolderName = Yii::getPathOfAlias('webroot')."/images/projects/project_".$model->url;
        if(is_dir($folderToBeRenamed))
            if(rename($folderToBeRenamed, $newFolderName))
                $model->addError('slide_order', "Error while renaming folders ! Please Try Again");

    }

    private function createSlide($model, $item) {
        $slide = new Slides();
        foreach ($model as $key => $value) {
            $slide->$key = $value;
        }
        if($slide->save()) {
            $this->renameSlideFolder($slide, $item, $slide->slide_order, true);
            if(strlen($slide->file) > 0)
                $this->saveSlideImage($slide, $item, $slide->file);
            if(strlen($slide->fullscreen) > 0)
                $this->saveSlideImage($slide, $item, $slide->fullscreen, true);
            $this->renderJson(array('success'=>true, 'slide' => $slide, 'item' => $item));
        }

    }
        
    public function actionCreatse()
	{ 

         
        if(isset($_POST['Items']) && $_POST['Slides']) {
                
                 $file = CUploadedFile::getInstanceByName("thumbnail");
              
                 if(!empty($file))
                     $image = $file;
                 else 
                     $image = $items->thumbnail; 
 
                 $items->attributes = $_POST['Items'];
                 $items->date_created = $_POST['Items']['date_created'];
                 $items->thumbnail = $image;
                 $items->url = $this->friendlyURL($items->name);

                 if ($items->save()) {
                     if(isset($_POST['Slides'])) {
                         foreach ($_POST['Slides'] as $i => $post) {

                            if($_POST['Slides'][$i]['status'] !== 'delete') {

                                 if($i >= count($slides)) {
                                    $slide =new Slides();
                                 } else {
                                     $slide = $slides[$i];
                                 }

                                 $moveFile = false;
                                 $moveFullscreenFile = false;
                                 
                                 $slide_file = $_POST['Slides'][$i]["file"]; 
                                 $slide_fullscreen_file = $_POST['Slides'][$i]["fullscreen"];

                                 if(strlen($slide_file) > 0)
                                    $moveFile = 1;
                                 if(strlen($slide_fullscreen_file) > 0)
                                    $moveFullscreenFile = 1;
                                
                                 $slide->ID_item = $items->ID;
                                 $slide->slide_order = $_POST['Slides'][$i]["slide_order"];
                                 $slide->type = $_POST['Slides'][$i]['type'];
                                 $slide->file = $slide_file;
                                 $slide->fullscreen = $slide_fullscreen_file;
                                 $slide->name =$_POST['Slides'][$i]["name"];        
                                 
                                 
                                 if ($slide->save()) {
                                     
                                     $dir = Yii::getPathOfAlias('webroot')."/images/projects/project_".$items->url."/slide_".$slide->slide_order."_".$slide->ID;
                                     $temp = Yii::getPathOfAlias('webroot')."/images/temp/slideimage_".$items->ID."_$slide_file";
                                     $dir_fullescreen = Yii::getPathOfAlias('webroot')."/images/projects/project_".$items->url."/slide_".$slide->slide_order."_".$slide->ID."/fullscreen";
                                     $temp_fullscreen = Yii::getPathOfAlias('webroot')."/images/temp/fullscreen/fullscreen_".$items->ID."_slideimage_new_$slide_fullscreen_file";
                                     
                                     if(!mkdir($dir, 0777, true))
                                        $slide->addError('ID', "Error Creating Folder for this slide ID: $slide->ID Name: $slide->name !");
                                     if(!mkdir($dir_fullescreen, 0777, true))                   
                                        $slide->addError('ID', "Error Creating Folder for this slides Fullscreen Image ID: $slide->ID Name: $slide->name !");
                                     
                                     if($moveFile) {
                                         if(!copy($temp, $dir."/".$slide_file))
                                            $slide->addError('file', "Error while uploading $dir.$slide_file $temp  ! Please Try Again");
                                         else
                                            unlink($temp);
                                     }

                                     if($moveFullscreenFile) {
                                         if($slide->type == "image")
                                             if(!copy($temp_fullscreen, $dir_fullescreen."/".$slide_fullscreen_file))
                                                    $slide->addError('file', "Error while uploading $temp_fullscreen.$slide_fullscreen_file $temp_fullscreen  ! Please Try Again");
                                                 else
                                                    unlink($temp_fullscreen);
                                     }
                                     
                                 }
                             }
                             
                         }
                     }
                     
                     if(!empty($file))
                         if(!$file->saveAs(Yii::getPathOfAlias('webroot')."/images/projects/project_".$items->url."/".$file))
                                 $items->addError ('thumbnail', "Error While Uploading Thumbnail picture");
                     
                     $this->redirect(Yii::app()->createUrl('Admin/default'));
                     
                 }
                     
            }
            
            $this->render('form', array("items"=>$items, "slides"=>$slides, 'credentials' => $this->credentials));
	}
        
        function deleteDir($directory) {
           if (is_dir($directory))
                 $dir = opendir($directory);
           else
              return false;
          
           while($file = readdir($dir)) {
          
              if ($file != "." && $file != "..")
           {
               //chmod($directory.$file, 0777);

                 if (!is_dir($directory."/".$file))
                    unlink($directory."/".$file);
                 else
                    $this->deleteDir($directory.'/'.$file);   
              }
           }
           closedir($dir);
           rmdir($directory);
           return true;
        }
        
        public function actionDeleteAll()
	{
			// we only allow deletion via POST request
			//Items::model()->findAll()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(Yii::app()->createUrl('Admin/default'));
        }

        public function actionTest($id, $delete = false)            
    {   

            $item = Items::model()->findByPk($id);
            $output = 'Project "'.$item->name.'" with ID: '.$item->ID.' has been deleted';
            
            $this->render('test', array('id'=>Yii::getPathOfAlias('webroot')."/images/projects/project_".$item->url, 'delete'=>1));
        }
        
        public function actionDelete($id, $delete = false)            
    {   

            $item = Items::model()->findByPk($id);
            $output = 'Project "'.$item->name.'" with ID: '.$item->ID.' has been deleted';
            
            if ($delete) {
                $dir = Yii::getPathOfAlias('webroot')."/images/projects/project_".$item->url;
                $this->deleteDir($dir);
                $output = $output.' completely from the server';
            }
            foreach ($item->slide as $slide) { 
                $slide->delete();
            }
            $item->delete();

            if(Yii::app()->request->isAjaxRequest)
                echo CJSON::encode(array(
                    "succsess"=>$id, 
                    "output"=>$output,  
                    'callbackUrl'=> Yii::app()->createUrl("Admin/default/items")
                ));
            else
                $this->redirect(Yii::app()->createUrl('Admin/default'));
        }
        
        
        public function actionUpdateCategory($id) {
                
                $category = Categories::model()->findByPk($id);
                
                if(isset($_POST['Categories'])) {
                    
                    $category->attributes = $_POST['Categories'];
                    if($category->validate()){
                        if($category->save())
                            $this->redirect(Yii::app()->createUrl('Admin/default/categories'));
                    }
                    
                }
                         
                $this->render('category', array("cat"=>$category));
                
        }
        
        public function actionCreateCategory() {
                
                $category = new Categories();
                
                if(isset($_POST['Categories'])) {
                    
                    $category->attributes = $_POST['Categories'];
                    if($category->validate()){
                        if($category->save())
                            $this->redirect(Yii::app()->createUrl('Admin/default/categories'));
                    }
                    
                }
                         
                $this->render('category', array("cat"=>$category));
                
        }

        public function actionOrderCategory() {
            if(isset($_POST["Category"])) {

                foreach ($_POST["Category"] as $i => $val) {
                    $category = Categories::model()->findByPk($i);
                    $category->cat_order = $val['cat_order'];
                    $category->save();
                }

                if(Yii::app()->request->isAjaxRequest)
                    echo CJSON::encode(array('output' => "Updated successfully !"));
                else
                    $this->redirect(Yii::app()->createUrl('Admin/default/categories'));
            }
         }
        
        public function actionDeleteCategory($id)
	{
			// we only allow deletion via POST request
                        $items = Items::model()->findAll();
                        
                        foreach ($items as $item) {
                            $data = Array();
                            $ar = split(";", $item->ID_category);
                            foreach ($ar as $cat) {
                                if ($cat != $id)
                                    array_push ($data, $cat);
                            }
                            $item->ID_category = join(";", $data);
                            $item->save();
                        }
                        
                        Categories::model()->findByPk($id)->delete();
                        
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(Yii::app()->createUrl('Admin/default/categories'));
        }

        public function actionPublish($id) {
            if(isset($_POST["publish"])) {

                $value = $_POST["publish"];
                $item = Items::model()->findByPk($id);
                
                $item->published = $value;
                if($item->save()){
                    if($value)
                        $output = 'Project "'.$item->name.'" with ID: '.$item->ID.' is now visible';                        
                    else
                        $output = 'Project "'.$item->name.'" with ID: '.$item->ID.' is now hidden';
                        
                } else
                    $output = 'Error Occured While Saving The Project !';

                if(Yii::app()->request->isAjaxRequest)
                    echo CJSON::encode(array( 
                        "output"=>$output,
                        "post" => $_POST["publish"]
                ));
                else
                    $this->redirect(Yii::app()->createUrl('Admin/default/categories'));
            }
         }

         public function actionPublishCategory($id) {
            if(isset($_POST["publish"])) {

                $value = $_POST["publish"];
                $item = Categories::model()->findByPk($id);
                
                $item->published = $value;
                if($item->save()){
                    if($value)
                        $output = 'Category "'.$item->category.'" with ID: '.$item->ID.' is now visible';                        
                    else
                        $output = 'Category "'.$item->category.'" with ID: '.$item->ID.' is now hidden';
                        
                } else
                    $output = 'Error Occured While Saving The Category !';

                if(Yii::app()->request->isAjaxRequest)
                    echo CJSON::encode(array( 
                        "output"=>$output,
                        "post" => $_POST["publish"]
                ));
                else
                    $this->redirect(Yii::app()->createUrl('Admin/default'));
            }
         }

    protected function friendlyUrl ($str = '') {
        $friendlyURL = htmlentities($str, ENT_COMPAT, "UTF-8", false); 
        $friendlyURL = preg_replace('/&([a-z]{1,2})(?:acute|lig|grave|ring|tilde|uml|cedil|caron);/i','\1',$friendlyURL);
        $friendlyURL = html_entity_decode($friendlyURL,ENT_COMPAT, "UTF-8"); 
        $friendlyURL = preg_replace('/[^a-z0-9-]+/i', '-', $friendlyURL);
        $friendlyURL = preg_replace('/-+/', '-', $friendlyURL);
        $friendlyURL = trim($friendlyURL, '-');
        $friendlyURL = strtolower($friendlyURL);
        return $friendlyURL;
    }
                
}