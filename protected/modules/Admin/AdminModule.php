<?php

class AdminModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'Admin.models.*',
			'Admin.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
                        $route=$controller->id.'/'.$action->id;
			$publicPages=array(
				'default/login',
				'default/error',
			);

			if(!UserModule::isAdmin() && Yii::app()->user->isGuest && !in_array($route,$publicPages)){
				$controller->redirect(array('/user/login'));
			}
				
			else
				return true;
		}
		else
			return false;
	}
}
