<?php
        foreach ($items as $item) {
            $hidden = 'show';
            if($item->published) $hidden = 'hide';
            echo "<div class='item'>";
            echo "<div class='item-id cell'>".$item->ID."</div>";
            echo "<div class='item-name cell'>".$item->name."</div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a href='".Yii::app()->createUrl("Admin/default/view", array("id"=>$item->ID))."'>
                            VIEW
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a href='".Yii::app()->createUrl("Admin/default/update", array("id"=>$item->ID))."'>
                            EDIT
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a href='".Yii::app()->createUrl("Admin/default/delete", array("id"=>$item->ID))."' onClick='javascript:showDialog(event, this.href)'>
                            DELETE
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a class='$hidden' href='".Yii::app()->createUrl("Admin/default/publish", array("id"=>$item->ID))."' onClick='javascript:publish(event, this.href, this)'>
                             $hidden
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-date cell'>".$item->date_created."</div>";
            echo "</div>";
    }

?>
