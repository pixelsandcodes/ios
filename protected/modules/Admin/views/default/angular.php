<!doctype html>
<html ng-app="form">
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.6/angular.min.js"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/js/angular.js"></script>
  </head>
  <body>
    <div>
      <label>Name:</label>
      <input type="text" ng-model="yourName" placeholder="Enter a name here">
      <hr>
      <h1>Hello {{yourName}}!</h1>
      <h1>{{ this }}!</h1>
    </div>
    <?php echo Yii::app()->createUrl('slides/item', array('id'=>1)) ?>
    <div ng-controller="ModelCtrl">
    	<p>ID: {{ item.ID }}</p>
    	<p>ID: {{ item.name }}</p>
    	<p>ID: {{ item.thumbnail }}</p>
    	<ul ng-repeat="slide in item.slide">
    		<li>{{ slide.ID }} {{ slide.name }} </li>
    	</ul>

    	Add new slide : <br/>
    	<button  ng-click="handleClick()" >New Slide</button>
    </div>

  </body>
</html>