

<?php 

    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/ckeditor/ckeditor.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/lib/underscore.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/lib/backbone.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/lib/form.js');

     $form=$this->beginWidget('CActiveForm', array(
	'id'=>'item-form',        
	'stateful'=>true,
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('name' => 'Items','enctype' => 'multipart/form-data')
)); ?>

    <?php echo $form->errorSummary($items ); ?>

    <?php echo $form->errorSummary($slides ); ?>

    <div class="button gray-bg rounded-corners back small">    
        <a href='<?php echo Yii::app()->createUrl('Admin/default/index') ?>'>
            back to projects
        </a>
    </div>
    <div class="detailed-item" id="<?php echo $items->ID ?>">

        <div class="half top">
            <div class="field small">
                 <p>Project Title</p>
                 <?php echo $form->textField($items,'name',array('maxlength'=>500, 'name'=>'name')); ?>
            </div>
            
            <div class="half">
                <img class="project-image" src="<?php echo Yii::app()->baseUrl.'/images/projects/project_'.$items->url.'/'.$items->thumbnail ?>" />

            </div>
            <div class="half top">
                <p>Project Category</p>
                <div class="categories-wrapper sortable draggable rounded-corners selected">
                    <?php 
                        $categories = split(";", $items->ID_category);                
                        foreach ($categories as $id) {
                            $category = Categories::model()->findByPk($id);
                            if(!empty ($category))
                                echo "<p id='$id' class='rounded-corners'>$category->category</p>";
                        }
                    ?>
                </div>
                <div class='button gray-bg rounded-corners' >
                    <a id="add-category" href="<?php echo Yii::app()->createUrl('Admin/default/createCategory') ?>">
                        assign a category
                    </a>
                </div>
                <div id="select-categories">
                    <h3>All categories</h3>
                    <p>Drag to add or to remove a category<p>
                
                    <div class="categories-wrapper sortable draggable rounded-corners all">
                        <?php 
                            $model =Categories::model()->findAll();                
                            foreach ($model as $cat) {                 
                                echo "<p id='$cat->ID' class='rounded-corners'>$cat->category</p>";
                            }
                        ?>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="half top">
            <div class="field">
                <p>Project Summary Text</p>
                <?php echo $form->textarea($items,'summary', array('rows'=>18, 'name'=>'summary')); ?>
            </div>
            <div class="field">
                <p>Project URL</p>
                <?php echo $form->textField($items,'link',array('size'=>60, 'name'=>'link', 'maxlength'=>500)); ?>
            </div>
            <div class="field">
                <p>Date</p>
                <?php echo $form->textField($items,'date_created',array('name'=>'date_created', 'id'=>'datepicker')); ?>
            </div>
        </div>
        <div class="half bottom">
             <div class="half bottom">
                <div class="button gray-bg rounded-corners back filefield">
                    Add Image
                </div> 
             </div>
             <div class="half bottom">
                 <div class="field small">
                    <p>Project Text</p>
                 </div>
             </div>
        </div>
        <div class="textfield field">
            <?php echo $form->textarea($items,'text', array('name'=>'text', 'rows'=>16)); ?>
        </div>
        <div class="field">
            <p>Project Slide List</p>
        </div>
        <ul class="slides-admin items" id="sortable">
            
        </ul>
        <!-- Clone Markup -->
        <script type="text/html" id="slide-template">
            <div class="item-id cell order">
                <%= slide.slide_order %>
            </div>
            <div class="item-name cell">
                <div class="field"> 
                <input type="text" name="name_slide" value="<%= slide.name %>" />                    
                </div>
            </div>
            <div class="item-inputs cell"> 
                <div class='button gray-bg rounded-corners'>
                    <a href='javascript:void(0)' data-image='file' class="inputs">
                        <p>upload</p>
                    </a>                    
                </div>
                <div class='hidden-fields'>
                </div>
            </div>
            <div class="item-inputs cell"> 
                <div class='button gray-bg rounded-corners'>
                    <a href='javascript:void(0)' data-image='fullscreen' class="inputs">
                        <p>fullscreen</p>
                    </a>                    
                </div>
                <div class='hidden-fields'>
                </div>
            </div>
            <div class="item-view cell">
                <div class="button gray-bg rounded-corners">
                    <a href="javascript:void(0)" class="toggle-type">
                        <%= slide.type %>
                    </a>                    
                </div>
            </div>
            <div class="item-view cell">
                <div class="button gray-bg rounded-corners">
                    <a href="<?php echo Yii::app()->baseUrl.'/images/projects/project_'.$items->url.'/slide_' ?><%= order + "_" + slide.ID %>" class='view-image'>
                        VIEW
                    </a>
                </div>
           </div>
           <div class="item-delete cell">
               <div class="button gray-bg rounded-corners">
                   <a href='javascript:void(0)' class='delete-slide'>
                        DELETE
                   </a>
               </div>               
           </div>
        </script>
        <!-- End Clone Markup -->
        
        <div class="button gray-bg rounded-corners back small">    
              <a href='javascript:void(0)' id="addSlide">
                Add a slide
             </a>
        </div>    

        <div class="button gray-bg rounded-corners back small"> 
            <a href='javascript:void(0)' id="delete-all">
                Delete all slides
            </a>
        </div>
    </div>

    <div class="admin-buttons">
        <div class="cells">
            <?php echo CHtml::submitButton('Save', array('id'=>'submitButton', 'class'=>'button gray-bg rounded-corners')); ?>
        </div>
    </div>

    </div>

<?php $this->endWidget(); ?>
        <!-- form -->
    <div id="loaderImage"></div>
    <script type="text/html" id="popup-template">
        <div class="popup-bg">
        </div>
        <div class="popup-placeholder gray-bg rounded-corners" id="popup-placeholder">
        </div>
    </script>

    <script type="text/html" id="save-popup-template">
        <div class="popup-bg">
        </div>
        <div class="save-popup-placeholder gray-bg rounded-corners" >
        <div id="save-popup-body">
            <h1>Saving Progress</h1>
        </div>
        <div id="save-popup-buttons">
        </div>            
        </div>
    </script>

    <script type="text/html" id="upload-template">
        <img class="close-icon" src="<?php echo Yii::app()->baseUrl ?>/png/text-overlay-large-x.png">
        <div class="file-upload">
            <div class="file-upload-div">
                <input type="file" name="file_field" />
                <div id="output"> </div>
                <div id="drop-area" class="drop-field rounded-corners">
                    Drop File Here
                </div>
            </div>
        </div>
        <div id="file-preview" class="file-preview">
            <img class='image-preview' />
        </div>
    </script>
        
<script>

    $(function() {
        window.formApp = new FormApp({<?php if($items->ID) echo "id : $items->ID ,"  ?> credentials : <?php echo CJSON::encode($credentials); ?>, root : '<?php echo Yii::app()->baseUrl ?>' });
    })
    
    // Drag And Drop
    
    $(".draggabpe p").draggable({
    	connectToSortable: '.sortable',
    	cursor: 'move',
    	cursorAt: { top: 0, left: 0 },
    	helper: 'clone',
    	revert: 'invalid'
    });

    $(".sortable").sortable({
    	connectWith: '.sortable',
    	cursor: 'move', 
    	cursorAt: { top: 0, left: 0 }, 
    	placeholder: 'ui-sortable-placeholder',
    	tolerance: 'pointer',
    	stop: function(event, ui) {
            formApp.view.assignCategories(event, ui);
            /*var ids = new Array();
            $(".categories-wrapper.selected p").each(function (){
                ids.push($(this).attr('id'));
            })
            console.log(ids, ids.join(';'))
            $("#Items_ID_category").val(ids.join(';'));*/
    	}
    });
    
        

</script>

<script type="text/javascript">
	var cSpeed=4;
        var cWidth=32;
        var cHeight=32;
        var cTotalFrames=18;
        var cFrameWidth=32;
        var cImageSrc='<?php echo Yii::app()->request->baseUrl; ?>/images/sprites.png';

        var cImageTimeout=false;

        function startAnimation(){

                document.getElementById('loaderImage').innerHTML='<canvas id="canvas" style="display:block;position:fixed;z-index:1;left:50%;top:50%" width="'+cWidth+'" height="'+cHeight+'"><p>Your browser does not support the canvas element.</p></canvas>';

                //FPS = Math.round(100/(maxSpeed+2-speed));
                FPS = Math.round(100/cSpeed);
                SECONDS_BETWEEN_FRAMES = 1 / FPS;
                g_GameObjectManager = null;
                g_run=genImage;

                g_run.width=cTotalFrames*cFrameWidth;
                genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
                initCanvas();
        }


        function imageLoader(s, fun)//Pre-loads the sprites image
        {
                clearTimeout(cImageTimeout);
                cImageTimeout=0;
                genImage = new Image();
                genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
                genImage.onerror=new Function('alert(\'Could not load the image\')');
                genImage.src=s;
        }

        //The following code starts the animation
        new imageLoader(cImageSrc, 'startAnimation()');
</script>