<h1><?php echo $model->category ?></h1>

<div class="items" >
    <?php 
        foreach ($model->items as $item) {
            echo "<div class='item'>";
            echo "<div class='item-id cell'>".$item->ID."</div>";
            echo "<div class='item-name cell'>".$item->name."</div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a href='".Yii::app()->createUrl("Admin/default/view", array("id"=>$item->ID))."'>
                            VIEW
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a href='".Yii::app()->createUrl("Admin/default/update", array("id"=>$item->ID))."'>
                            EDIT
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a href='".Yii::app()->createUrl("Admin/default/delete", array("id"=>$item->ID))."'>
                            DELETE
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-date cell'>".$item->date_created."</div>";
            echo "</div>";
    }

    ?>
    
</div>
