<h1>MASS CMS</h1>
<h3>Project List:</h3>
<h2 id="json-result" class="error"><?php echo $output; ?></h2>
<div class="items" >
    <?php 
        foreach ($items as $item) {
            $hidden = 'show';
            if($item->published) $hidden = 'hide';
            echo "<div class='item'>";
            echo "<div class='item-id cell'>".$item->ID."</div>";
            echo "<div class='item-name cell'>".$item->name."</div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a href='".Yii::app()->createUrl("Admin/default/view", array("id"=>$item->ID))."'>
                            VIEW
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a href='".Yii::app()->createUrl("Admin/default/update", array("id"=>$item->ID))."'>
                            EDIT
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a href='".Yii::app()->createUrl("Admin/default/delete", array("id"=>$item->ID))."' onClick='javascript:showDialog(event, this.href)'>
                            DELETE
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a class='$hidden' href='".Yii::app()->createUrl("Admin/default/publish", array("id"=>$item->ID))."' onClick='javascript:publish(event, this.href, this)'>
                            $hidden
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-date cell'>".$item->date_created."</div>";
            echo "</div>";
    }

    ?>
    
</div>
<div class="admin-buttons">
    <div class="cells">
        <div class='button gray-bg rounded-corners'>
            <a href='<?php echo Yii::app()->createUrl("Admin/default/create") ?>'>
                Add a new project
            </a>            
        </div>
    </div>
    
    <div class="cells">
        <div class='button gray-bg rounded-corners'>
            <a href='<?php echo Yii::app()->createUrl("Admin/default/deleteall") ?>'>
                Delete All projects
            </a> 
        </div>
    </div>
    
    
    <div id="select" >
        <select class="rounded-corners button gray-bg clean-border" id="sort">
		  <option>Sort by</option>
		  <option value="<?php echo Yii::app()->createUrl("Admin/default/sort", array("id"=>'0')) ?>">List order</option>
		  <option value="<?php echo Yii::app()->createUrl("Admin/default/sort", array("id"=>'1')) ?>">Alphabetical</option>
		  <option value="<?php echo Yii::app()->createUrl("Admin/default/sort", array("id"=>'2')) ?>">Date created</option>
		  <option value="<?php echo Yii::app()->createUrl("Admin/default/sort", array("id"=>'3')) ?>">Random</option>
		</select>
    </div>
</div>

<div class="admin-buttons">
    <div class="cells">
        <div class='button gray-bg rounded-corners'>
            <a href='<?php echo Yii::app()->createUrl("Admin/default/categories") ?>'>
                Manage Categories
            </a>            
        </div>
    </div>
</div>

<div id="dialog" class='rounded-corners shadow'>
    <p class="title">Delete Project</p>
    <p>Do you wish to delete the project folder ?<p>
    <div class="table" >
        <div class='cell' >
            <div class='button gray-bg rounded-corners'>                        
                <a id="Yes" href='javascript:void(0)' onClick='javascript:confirmDelete(this)'>
                    Yes
                </a>
            </div>
        </div>
        <div class='cell' >
            <div class='button gray-bg rounded-corners'>                        
                <a id="No" href='javascript:void(0)' onClick='javascript:confirmDelete(this)'>
                    No
                </a>
            </div>
        </div>
        <div class='cell'>
            <div class='button gray-bg rounded-corners'>                        
                <a id="Cancel" href='javascript:void(0)' onClick='javascript:confirmDelete(this)'>
                    Cancel
                </a>
            </div>
        </div>
    </div>
</div>
 
<script>

$(function() {

    $('#json-result').delay(2000).fadeOut(600)

     
});

function showDialog(e, url) {
    e.preventDefault();
    $("#dialog").attr('data-url', url).show();
}

function confirmDelete(el) {
    url = $("#dialog").attr('data-url');
    switch (el.id) {
        case 'Yes': return getJSON(url+"?delete=1");break;
        case 'No': return getJSON(url);break;
        case 'Cancel': return $("#dialog").hide();break;
    }        
    return false;
}

function getJSON(url) {
    ;
    $.getJSON(url, function(data) { 
        $("#dialog").hide();     
        loadItems(data.callbackUrl, function() {           
            $('#json-result').stop().html(data.output).fadeIn().delay(4000).fadeOut(600)
        })         
        return false;          
    });
    return false;
}

</script>
