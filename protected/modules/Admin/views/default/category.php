<div>
    
    <div class="button gray-bg rounded-corners back small">    
        <a href='<?php echo Yii::app()->createUrl('Admin/default/categories') ?>'>
            back to categories
        </a>
    </div>
    
    <h1>Manage Category</h1>
       
<?php
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-form',
        'enableAjaxValidation'=>false,
));  
?>

    <div class="detailed-item">
        
         <?php echo $form->errorSummary($cat ); ?>
        <div class="half top">
            <div class="field">
                <p>Category Name</p> <p>
                 <?php echo $form->textField($cat,'category',array('maxlength'=>500)); ?>
                <p class="error"></p>

            </div>
            <div class="half top">
            <?php echo CHtml::submitButton($cat->isNewRecord ? 'Create' : 'Save', array(
                    "class"=> "button gray-bg rounded-corners back small",
                )); ?>
            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>
</div>