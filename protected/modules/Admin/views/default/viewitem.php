<div class="button gray-bg rounded-corners back small">    
    <a href='<?php echo Yii::app()->createUrl('Admin/default/index') ?>'>
        back to projects
    </a>
</div>
<div class="button gray-bg rounded-corners back small">    
    <a href='<?php echo Yii::app()->createUrl('Admin/default/update', array("id"=>$item->ID)) ?>'>
        Edit Project
    </a>
</div>

<div class="detailed-item">
    
    <div class="half">
        <div class="field">
            <h1><?php echo $item->name ?></h1>
        </div>
        <div class="half bottom">
            <img src="<?php echo Yii::app()->baseUrl.'/images/projects/project_'.$item->url.'/'.$item->thumbnail ?>" />    
        </div>
        <div class="half bottom">
            <div class="field">
                <p>Project Category</p>
                <?php 
                    $categories = split(";", $item->ID_category);                
                    foreach ($categories as $id) {
                        $category = Categories::model()->findByPk($id);                    
                        echo "<p class='value rounded-corners gray-bg'>$category->category</p>";
                    }
                ?>
                
            </div>
        </div>
    </div>
    <div class="half top">
        <div class="field">
            <p>Project Summary Text</p>
            <div class="value rounded-corners gray-bg"><?php echo $item->summary ?></div>
        </div>
        <div class="field">
            <p>Project URL</p>
            <p class="value rounded-corners gray-bg"><?php echo $item->link ?></p>
        </div>        
    </div>
    <div class="half bottom">
        <div class="half top">
            <p></p>
        </div>
        <div class="half bottom">
            <div class="field">
                <p>Project Text</p>
            </div>            
        </div>
    </div>
   <div class="textfield field">
        <div class="value rounded-corners gray-bg"><?php echo $item->text ?></div>
    </div>
    <div class="field">
        <p>Project Slide List</p>
    </div>
    <div class="slides-admin items">
        <?php 
        
            foreach ($item->slide as $slide) { 
                echo "<div class='item'>";
                echo "<div class='item-id cell'>".$slide->slide_order."</div>";
                echo "<div class='item-name cell'>".$slide->name."</div>";
                echo "<div class='item-view cell'>
                        <p></p>
                      </div>";
                echo "<div class='item-view cell'>
                        <p></p>
                      </div>";
                echo "<div class='item-view cell'>
                        <div class='button gray-bg rounded-corners'>
                            <p>
                                $slide->type
                            </p>
                        </div>
                      </div>";
                echo "<div class='item-view cell'>
                        <div class='button gray-bg rounded-corners'>
                            <a class='view-image' href='".Yii::app()->baseUrl."/images/projects/project_".$item->url."/slide_".$slide->slide_order."_$slide->ID/".$slide->file."'>
                                VIEW
                            </a>
                        </div>
                      </div>";
                echo "</div>";
            }
        
        ?>
    </div>
    
</div>
<div id="loaderImage"></div> 
<div id="image-popup" class="image-popup" style="display:none">
    <div class="popup-bg">
    </div>
    <div class="popup-placeholder gray-bg rounded-corners" id="popup-placeholder">
    </div>
</div>

</div>

<script>

    $(document).click(function() {
        $(".image-popup").fadeOut(200)
    })

    $(".view-image").click(function(e) {
        
        $("#loaderImage").fadeIn(150);
        
        e.preventDefault();
        var image = new Image();
        $(image).hide();
        image.src = this.href;

        $(image).bind("load",function() {
            
            if(image.width > window.innerWidth * 0.7 || image.height > window.innerHeight * 0.8) {

                var ratio = image.height * window.innerWidth * 0.7 / image.width < window.innerHeight * 0.8? 
                    window.innerWidth * 0.7 / image.width :
                    window.innerHeight * 0.8 / image.height

                var width = ratio * image.width;
                var height = ratio * image.height;

                image.height = height;
                image.width = width;
            }
            
            $(this).fadeIn(300, function() {
                $("#popup-placeholder").html(image);
                $("#popup-placeholder").css({
                    top  : (window.innerHeight - image.height) / 2 - 10,
                    left : (window.innerWidth - image.width) / 2 - 10
                 });
                $(".image-popup").fadeIn(200, function() {
                    $("#loaderImage").fadeOut(150);
                });
            })        
        })


    })

</script>

<script type="text/javascript">
	var cSpeed=4;
        var cWidth=32;
        var cHeight=32;
        var cTotalFrames=18;
        var cFrameWidth=32;
        var cImageSrc='<?php echo Yii::app()->request->baseUrl; ?>/images/sprites.png';

        var cImageTimeout=false;

        function startAnimation(){

                document.getElementById('loaderImage').innerHTML='<canvas id="canvas" style="display:block;position:fixed;z-index:1;left:50%;top:50%" width="'+cWidth+'" height="'+cHeight+'"><p>Your browser does not support the canvas element.</p></canvas>';

                //FPS = Math.round(100/(maxSpeed+2-speed));
                FPS = Math.round(100/cSpeed);
                SECONDS_BETWEEN_FRAMES = 1 / FPS;
                g_GameObjectManager = null;
                g_run=genImage;

                g_run.width=cTotalFrames*cFrameWidth;
                genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
                initCanvas();
        }


        function imageLoader(s, fun)//Pre-loads the sprites image
        {
                clearTimeout(cImageTimeout);
                cImageTimeout=0;
                genImage = new Image();
                genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
                genImage.onerror=new Function('alert(\'Could not load the image\')');
                genImage.src=s;
        }

        //The following code starts the animation
        new imageLoader(cImageSrc, 'startAnimation()');
</script>