<h1>MASS CMS</h1>
<h3>Category List:</h3>
<h2 id="json-result" class="error"></h2>

<?php 

     $form=$this->beginWidget('CActiveForm', array(
        'id'=>'categories-form',        
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('name' => 'Category')
    )); 

?>


<?php echo $form->errorSummary($model); ?>


<ul class="items" id="sortable">
    <?php 
        foreach ($model as $cat) {
            $hidden = 'show';
            if($cat->published) $hidden = 'hide';
            echo "<li class='item'>";
            echo "<div class='item-id cell'>
                    <span>".$cat->cat_order."</span>";
                echo CHtml::hiddenField("Category[$cat->ID][cat_order]", $cat->cat_order, array('class'=>'order-field'));
            echo "</div>";
            echo "<div class='item-name cell'>".$cat->category."</div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a href='".Yii::app()->createUrl("Admin/default/viewcategory", array("id"=>$cat->ID))."'>
                            VIEW
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a href='".Yii::app()->createUrl("Admin/default/updatecategory", array("id"=>$cat->ID))."'>
                            EDIT
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a href='".Yii::app()->createUrl("Admin/default/deletecategory", array("id"=>$cat->ID))."'>
                            DELETE
                        </a>
                    </div>
                  </div>";
            echo "<div class='item-view cell'>
                    <div class='button gray-bg rounded-corners'>
                        <a class='$hidden' href='".Yii::app()->createUrl("Admin/default/publishCategory", array("id"=>$cat->ID))."' onClick='javascript:publish(event, this.href, this)'>
                             $hidden
                        </a>
                    </div>
                  </div>";
            echo "</li>";
    }

    ?>
    
</ul>

    <div class="admin-buttons">
        <div class="cells">
            <?php echo CHtml::submitButton('Save', array('id'=>'submitButton', 'class'=>'button gray-bg rounded-corners')); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>

<div class="admin-buttons">
    <div class="cells">
        <div class='button gray-bg rounded-corners'>
            <a href='<?php echo Yii::app()->createUrl("Admin/default/createcategory") ?>'>
                Create new Category
            </a>            
        </div>
    </div>
</div>
<div class="admin-buttons">
    <div class="cells">
        <div class="button gray-bg rounded-corners back">    
            <a href='<?php echo Yii::app()->createUrl('Admin/default/index') ?>'>
                back to projects
            </a>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function() {

    $('#json-result').delay(2000).fadeOut(600)

     
});

$("#submitButton").click(function(e) {
    e.preventDefault();

    $.ajax({
        url: '<?php echo Yii::app()->createUrl("Admin/default/ordercategory") ?>',
        data: $('#categories-form').serialize(),
        type: "POST",
        dataType: "json",
        success: function( data ) {
            $('#json-result').stop().html(data.output).fadeIn().delay(4000).fadeOut(600)
        },
        error: function(x, e, r) {
            console.log(x, e, r);
        }
    })

})

$( "#sortable" ).sortable({update : function() {
                
    var start = 1;
    $(this).children("li").each(function() {
        $(this).find(".item-id span").html(start);
        console.log($(this).find(".order-field"))
        $(this).find(".order-field").val(start);
        start++;
    })
}});

</script>