<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
?>

<div id="login-form">
    
    <h1><?php echo UserModule::t("Login"); ?></h1>

    <?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

    <div class="success">
            <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
    </div>

    <?php endif; ?>
    
    <?php echo CHtml::beginForm(); ?>
	
	<?php echo CHtml::errorSummary($model); ?>
	
	<div class="row">
                <label>Username</label>
		<?php echo CHtml::activeTextField($model,'username') ?>
	</div>
	
	<div class="row">
		<label>Password</label>
		<?php echo CHtml::activePasswordField($model,'password') ?>
	</div>
    
        <div class="rememberMe">
		<?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
		<?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::submitButton(UserModule::t("Login"), array("id"=>"submit-button","class"=>"form-button")); ?>
		<?php echo CHtml::button(UserModule::t("Forgot Password?"), array("submit"=> Yii::app()->getModule('user')->recoveryUrl, "class"=>"form-button big")); ?>
	</div>
	
    <?php echo CHtml::endForm(); ?>
</div><!-- form -->

<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);
?>