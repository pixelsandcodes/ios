<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class UserMainController extends CController
{

	public $layout='//layouts/admin';
        
	public $menu=array();

	public $breadcrumbs=array();

}