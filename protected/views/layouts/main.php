<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html itemscope='itemscope' itemtype='http://schema.org/WebPage'>
<head>
        <meta name="viewport" content="width=device-width; initial-scale=1; maximum-scale=1.0;">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
        <meta itemprop="description" name="description" content="MASS iPad Experience" />
        <meta itemprop="keywords" name="keywords" content="" />
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/video.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/mediaQueries.css" />
        
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/history.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/video.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/swipe.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/canvas.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.fittext.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/iscroll.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/Globe/Clock.js" type="text/javascript"></script> 
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/Globe/Vector2.js" type="text/javascript"></script> 
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/Globe/Vector3.js" type="text/javascript"></script> 
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/Globe/Matrix44.js" type="text/javascript"></script> 
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/Globe/Quat.js" type="text/javascript"></script> 
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/Globe/Vertex.js" type="text/javascript"></script> 
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/Globe/Matrix33A.js" type="text/javascript"></script> 
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/Globe/Face.js" type="text/javascript"></script> 
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/Globe/Globe.js" type="text/javascript"></script> 
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/Globe/Markers.js" type="text/javascript"></script>

	<title>Mass 3.0</title>
        <script type="text/javascript">
                var cSpeed=4;
                var cWidth=32;
                var cHeight=32;
                var cTotalFrames=18;
                var cFrameWidth=32;
                var cImageSrc='<?php echo Yii::app()->request->baseUrl; ?>/images/sprites.png';

                var cImageTimeout=false;

                function startAnimation(){

                        document.getElementById('loaderImage').innerHTML='<canvas id="canvas" width="'+cWidth+'" height="'+cHeight+'"><p>Your browser does not support the canvas element.</p></canvas>';

                        //FPS = Math.round(100/(maxSpeed+2-speed));
                        FPS = Math.round(100/cSpeed);
                        SECONDS_BETWEEN_FRAMES = 1 / FPS;
                        g_GameObjectManager = null;
                        g_run=genImage;

                        g_run.width=cTotalFrames*cFrameWidth;
                        genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
                        initCanvas();
                }


                function imageLoader(s, fun)//Pre-loads the sprites image
                {
                        clearTimeout(cImageTimeout);
                        cImageTimeout=0;
                        genImage = new Image();
                        genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
                        genImage.onerror=new Function('alert(\'Could not load the image\')');
                        genImage.src=s;
                }
                //The following code starts the animation
                new imageLoader(cImageSrc, 'startAnimation()');
        </script>
</head>

<body>
    <div id="loaderImage"></div>
    <aside id="categories" class="menu-item" data-width="0.3286">
        <div class="menu-item-wrapper categories position" >
            <?php            
             $model =Categories::model()->findAll(array('order' =>'cat_order ASC', 'condition'=>'published = 1'));
                echo $this->renderPartial('categories', array('model'=> $model));     
            ?>
        </div>
    </aside>
    
    <section id="about" class="menu-item" data-width="0.97">
        <div class="menu-item-wrapper about" >
            <?php echo $this->renderPartial('about'); ?>
        </div>
    </section>
    
    <aside id="contact" class="menu-item" data-width="0.66">
        <div class="menu-item-wrapper contact position" >
            <?php echo $this->renderPartial('contact', array('model'=> new ContactForm)); ?>
        </div>
    </aside>
    
        <nav id="mainmenu">
            <ul>
                <li class="m-icon-wrapper">
                    <a href="#about" class="m-icon">
                    </a>
                </li>
                <li class="contact-icon-wrapper">
                    <a href="#contact" class="contact-icon">
                    </a>
                </li>
                <li class="filter-icon-wrapper">
                    <a href="#categories" class="filter-icon">
                    </a>
                </li>
            </ul>
        </nav><!-- mainmenu -->

        <?php echo $content; ?>
 
	<div class="clear"></div>
    
    <aside id="fullscreen-wrapper"><div class="ajax"></div></aside>
    
</body>
 
</html>
