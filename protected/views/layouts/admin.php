<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width; initial-scale=1; maximum-scale=1.0;">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/canvas.js"></script>
        <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>

        <title>Mass 3.0 CMS</title>
        <style>
            body {
                overflow: auto;
            }
        </style>
        <script>
            $(document).ready(function() {
                // Admin Sort
                $("#sort").change(function () {
                    $("select option:selected").each(function () {
                        loadItems($(this).val())
                    });         
                })
            })

            function loadItems(url, callback) {
                $(".items").fadeOut(400)
                $.ajax({
                    url: url,
                    success: function(data){
                       $(".items").html(data).fadeIn(400, function() {
                        if(callback)
                            callback(data);
                       })
                    }
                });               
            }

            function publish(e, url, el) {
                e.preventDefault();

                $.ajax({
                    url: url,
                    data: { publish: el.className == "hide" ? 0 : 1},
                    type: "POST",
                    dataType: "json",
                    success: function( data ) {

                        var val = el.className == "hide" ? 'show' : 'hide';
                        el.className =  el.innerHTML = val;

                        $('#json-result').stop().html(data.output).fadeIn().delay(4000).fadeOut(600)
                    },
                    error: function(x, e, r) {
                        console.log(x, e, r);
                    }
                });

                return false;
            }
            
        </script>
    </head>
    <body>        
        <div id="wrapper">
            <div class="admin">
                <?php echo $content; ?>
            </div>            
        </div>
    </body>
</html>

