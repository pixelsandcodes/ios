
<section id="box-wrapper">
    <?php

        foreach ($model as $key => $item) {

            echo $this->renderPartial('box', array('item'=>$item));

        }

    ?>
    
    <figure class="box_item view-all animation" id="view-all" >
       <div class="original">   
            <div class="image">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true) ?>/png/view-all.png" title="View All>"/>
            </div>
       </div>
    </figure>
</section>
