<div class="filter">
    <div class="search">
      <form>
        <input id="search-filter" type="text" name="search" placeholder="SEARCH" onFocus="javascript:this.placeholder = ''" onBlur="javascript:this.placeholder = 'SEARCH'"/>
      </form>
    </div>
    <nav>
        <ul>
            <?php
                foreach ($model as $category) {
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'ID_category LIKE :cat';
                    $criteria->params = array(':cat'=>"%".$category->ID."%");
                    $items = Items::model()->findAll($criteria);
                    $published = 0;
                    foreach ($items as $item) {
                        if($item->published == 0)
                            $published++;
                    }
                    if($published !== count($items))                    
                        echo "<li><a href='javascript:void(0)' id='".$category->ID."-$category->category' rel='".$category->category."'>".$category->category."</a></li>";
                }
            ?>
            <li class="view-all"><a id="view-all-category" rel='View-All' class="selected" href='javascript:void(0)' >View All</a></li>
        </ul>
    </nav>    
</div>