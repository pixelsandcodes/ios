<?php $showFullText = strlen($item->text) > 0; ?>
<article class="detailed-item">
    <div class="closesign">
        <img src='<?php echo Yii::app()->request->getBaseUrl(true)."/png/window-overlay-x.png"; ?>' />
    </div>
    <section class="text-overlay animation">
        <div class="text-closesign">
            <img src='<?php echo Yii::app()->request->getBaseUrl(true)."/png/text-overlay-x.png"; ?>' />
        </div>
        <div class="text-wrapper fullscreen" itemprop="description">
            <div>
                <?php
                
                    $str = $item->summary;
                    echo substr($str, 0, strlen($str) - 4);
                    if($showFullText)
                        echo '<span><a class="read-more fullscreen"> READ MORE</a><span>';
                ?>
                </p> 
            </div>          
            <div class="itunes-link" itemprop="contentURL">
                <?php echo $item->link ?>
            </div>           
        </div>        
    </section>
    <div id="swipe-wrapper-fullscreen">
        <ul id="swipe-gallery">
        <?php

            foreach ($item->slide as $slide) { 
               $slideImage = Yii::app()->request->getBaseUrl(true)."/images/projects/project_".$item->url."/slide_".$slide->slide_order."_$slide->ID/fullscreen/".$slide->fullscreen;
                if($slide->type == "video") {
                    echo "<li class='video-slide' itemscope itemtype='http://schema.org/VideoObject' data-slide='".Yii::app()->createAbsoluteUrl('site/videos', array('id'=>$slide->ID))."' data-title='$slide->name'>";
                    echo "<div style='position: relative'>";
                    echo "<video id='video-fullscreen-$slide->ID' controls poster='".$slideImage."' preload='none' class='video-js vjs-default-skin' controls data-setup='{}'></video></div>";
                }
                else {
                    echo "<li itemscope itemtype='http://schema.org/ImageObject'>";
                    echo "<div style='position: relative'>";                   
                    echo "<img class='slide-image' itemprop='contentURL' src='".$slideImage."' /></div>";
                }
                echo "</li>";
           }

        ?>
        </ul>
    </div>  
    <div class="large-arrows animation <?php if(strstr($_SERVER['HTTP_USER_AGENT'],'iPad')) echo 'ipad' ?>" id="fullscreen-prev">
        <img src='<?php echo Yii::app()->request->getBaseUrl(true) ?>/png/arrows-large-left.png' />
    </div>
    <div class="large-arrows animation <?php if(strstr($_SERVER['HTTP_USER_AGENT'],'iPad')) echo 'ipad' ?>" id="fullscreen-next">
        <img src='<?php echo Yii::app()->request->getBaseUrl(true) ?>/png/arrows-large-right.png' />
    </div>
    <div class="bottom-links">
         <div id="navigation" class="<?php if(strstr($_SERVER['HTTP_USER_AGENT'],'iPad')) echo 'ipad' ?>">
            <?php
                foreach ($item->slide as $slide) {
                    echo "<li class='dots fullscreen-dots'></li>";
                }
            ?>
        </div>
    </div>
    
</article>

