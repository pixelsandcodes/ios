<article itemscope itemtype="http://schema.org/ContactPage"> 
    <div class="contact-map">
        <div id="globeCircle" >
            <img id="globeCircleImg" src="<?php echo Yii::app()->request->getBaseUrl(); ?>/png/globe-circle.png" width="500px"/>
        </div>
        <figure id='canvas-wrapper'>
                        
        </figure>

        <div class="marker" data-lat="34.052234" data-lng="-118.243684" data-id="la" ><img src="<?php echo Yii::app()->request->getBaseUrl(); ?>/png/marker.png" /></div>
        <div class="marker" data-lat="40.714623" data-lng="-74.006605" data-id="new-york"><img src="<?php echo Yii::app()->request->getBaseUrl(); ?>/png/marker.png" /></div>
        <div class="marker" data-lat="48.8567" data-lng="2.3508" data-id="berlin" ><img src="<?php echo Yii::app()->request->getBaseUrl(); ?>/png/marker.png" /></div>

    </div>
    <script type="text/javascript">

        var wrapper = document.getElementById("canvas-wrapper");
        var image = document.getElementById("globeCircleImg");
        var canvas = document.createElement('canvas');
        image.setAttribute("width", window.innerWidth * 0.405);
        image.setAttribute("height", window.innerWidth * 0.385);
        canvas.setAttribute("width", window.innerWidth * 0.40);
        canvas.setAttribute("height", window.innerWidth * 0.38);
        canvas.setAttribute("id", "globeCanvas");
        wrapper.appendChild(canvas);

        function initGlobeCanvas() {

            var marker = $('.marker');            
            var g = new Gl.Globe( document.getElementById('globeCanvas') );

            var globeImage = new Image();
            globeImage.onload = function() {
                g.SetImage(globeImage);
                g.RotateTo(40.714623, -74.006605)
            }

            globeImage.src = '<?php echo Yii::app()->request->getBaseUrl(); ?>/png/satellite.jpg';

            document.m   = new Gl.Vector3();
            document.out = new Gl.Vector3();
            var m = new Gl.Markers(g, marker.width() / 2, marker.height());

            m.AddMarkers(marker);
        }

    </script>

    <hgroup>
        <div id="new-york" class="city">
            <div class="cell headline">
                <h2 class="contact-talk">TALK TO US</h2>
            </div>
            <div class="cell details">
                <p><a href="#" target="_new">NEW YORK</a></p>
                <p><a href="javascript:void(0)">+1 (212) 989-6999</a></p>
                <br />
                <p><a href="mailto:info@mass.com">EMAIL</a></p>
                <p><a href="http://twitter.com/massdotcom"  target="_new">TWITTER</a></p>
                <p><a href="https://www.facebook.com/pages/MASSCOM/318242948033?fref=ts" target="_new">FACEBOOK</a></p>
            </div>        
        </div>
        <div id="la" class="city">
            <div class="cell headline">
                <h2 class="contact-talk">TALK TO US</h2>
            </div>
            <div class="cell details">
                <p><a href="#" target="_new">LA</a></p>
                <p><a href="javascript:void(0)">+1 (212) 989-6999</a></p>
                <br />
                <p><a href="mailto:info@mass.com">EMAIL</a></p>
                <p><a href="http://twitter.com/massdotcom" target="_new">TWITTER</a></p>
                <p><a href="https://www.facebook.com/pages/MASSCOM/318242948033?fref=ts" target="_new">FACEBOOK</a></p>
            </div>  
        </div>
        <div id="berlin" class="city">
            <div class="cell headline">
                <h2 class="contact-talk">TALK TO US</h2>
            </div> 
            <div class="cell details">
                <p><a href="#" target="_new">PARIS</a></p>
                <p><a href="javascript:void(0)">+1 (212) 989-6999</a></p>
                <br />
                <p><a href="mailto:info@mass.com">EMAIL</a></p>
                <p><a href="http://twitter.com/massdotcom" target="_new">TWITTER</a></p>
                <p><a href="https://www.facebook.com/pages/MASSCOM/318242948033?fref=ts" target="_new">FACEBOOK</a></p>
            </div>  
    </hgroup>
</article>