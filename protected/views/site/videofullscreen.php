<?php
    $types = array('video/ogg', 'video/webm', 'video/mp4');
    $videos = array();

    if ($handle = opendir(Yii::getPathOfAlias('webroot')."/images/projects/project_".$slide->item->name."/slide_".$slide->slide_order."_".$slide->ID)) {

        while (false !== ($entry = readdir($handle))) {
            $type = CFileHelper::getMimeTypeByExtension($entry);
            if(in_array($type, $types)) {
                array_push($videos, array('file'=>$entry, 'type'=>$type));
            }
        }

        closedir($handle);
    }
    
    $poster = Yii::app()->request->getBaseUrl(true)."/images/projects/project_".$slide->item->name."/slide_".$slide->slide_order."_".$slide->ID."/".$slide->file;

    echo "<video id='video_fullscreen_$slide->ID' poster='$poster' class='video-js vjs-default-skin' controls data-setup='{}' >";
    
    echo "</video>";
?>
<script>
    var fullscreenVideos = [
        <?php
             foreach ($videos as $vid) {
                $video = Yii::app()->request->getBaseUrl(true)."/images/projects/project_".$slide->item->name."/slide_".$slide->slide_order."_".$slide->ID."/".$vid['file'];
                $type = $vid['type'];
                echo "{type : '$type', src:'$video'},";
            }
        ?>
    ];
    var myPlayerFullscreen = _V_('video_fullscreen_<?php echo $slide->ID; ?>');
    myPlayerFullscreen.src(fullscreenVideos);

    myPlayerFullscreen.ready(function(){

        var player = this;    // Store the video object

        function resizeVideoJS(){
            // Get the window size
            var width = $(window).innerWidth();
            var height = $(window).innerHeight() - 0.92;
            // Set width and height
            player.width(width).height( height );
        }

        setTimeout(function() {
            resizeVideoJS(); // Initialize the function
        }, 400)
        window.onresize = resizeVideoJS; // Call the function on resize
    });
</script>

