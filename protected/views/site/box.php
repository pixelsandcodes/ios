<?php 

  $categories = '';
  foreach (split(";", $item->ID_category) as $e) {
    $cat = Categories::model()->findByPk($e);
    $categories = $categories." ".$cat->category;
  }
?>

<article 
    class="<?php echo $categories ?> box_item animation" 
    id="<?php echo $item->ID ?>" 
    data-name="<?php echo $item->name ?>"
    data-url="<?php echo Yii::app()->createAbsoluteUrl("site/detaileditem", array("id"=> $item->ID)) ?>"
    detailed-url="<?php echo Yii::app()->createAbsoluteUrl("site/fullscreenitem", array("id"=> $item->ID)) ?>"
    itemscope itemtype="http://schema.org/MediaObject">
    <div class="original">   
        <div class="image">
            <img itemprop="image" src="<?php echo Yii::app()->request->getBaseUrl(true)."/images/projects/project_$item->url/".$item->thumbnail ?>" alt="<?php echo Yii::app()->request->getBaseUrl(true)."/images/".$item->thumbnail ?>" title="<?php echo $item->name ?>"/>
        </div>
        <h2 itemprop="name" class="title"><?php echo $item->name ?></h2>
   </div>
   <section class="ajax">
   </section>
</article>