<?php $showFullText = strlen($item->text) > 0; ?>
<article class="detailed-item">
    <div class="closesign">
        <img src='<?php echo Yii::app()->request->getBaseUrl(true)."/png/tile-large-x.png"; ?>' />
    </div>
    <section class="text-overlay animation">
        <div class="text-closesign">
            <img src='<?php echo Yii::app()->request->getBaseUrl(true)."/png/text-overlay-x.png"; ?>' />
        </div>
        <div class="text-wrapper" itemprop="description">
            <div>
                <?php
                
                    $str = $item->summary;
                    echo substr($str, 0, strlen($str) - 4);
                    if($showFullText)
                        echo '<span><a class="read-more"> READ MORE</a><span>';
                ?>
                </p> 
            </div>          
            <div class="itunes-link" itemprop="contentURL">
                <?php echo $item->link ?>
            </div>           
        </div>        
    </section>
    <?php if($showFullText): ?>
        <section class="text-overlay-fullscreen animation" >
            <div class="text-closesign-fullscreen">
                <img src='<?php echo Yii::app()->request->getBaseUrl(true)."/png/tile-large-x.png"; ?>' />
            </div>
            <div class="text-wrapper" id='iScroll' itemprop="description" >
                <?php echo $item->text ?>
                <div class="itunes-link" itemprop="contentURL">
                    <?php echo $item->link ?>
                </div> 
            </div>

        </section>
    <?php endif; ?>
    <div id="swipe-wrapper">
        <ul id="swipe-gallery">
        <?php

            foreach ($item->slide as $slide) { 
                $slideImage = Yii::app()->request->getBaseUrl(true)."/images/projects/project_".$item->url."/slide_".$slide->slide_order."_$slide->ID/".$slide->file;
                if($slide->type == "video") {
                    echo "<li class='video-slide' itemscope itemtype='http://schema.org/VideoObject' data-slide='".Yii::app()->createAbsoluteUrl('site/videos', array('id'=>$slide->ID))."' data-title='$slide->name'>";
                    echo "<div style='position: relative'><video id='video-$slide->ID' controls poster='".$slideImage."' preload='none' class='video-js vjs-default-skin' controls data-setup='{}'></video><p class='slide-caption' itemprop='name'>$slide->name</p></div></li>";
                }
                else {
                    echo "<li itemscope itemtype='http://schema.org/ImageObject'>";                   
                    echo "<div style='position: relative'><img itemprop='contentURL' src='".$slideImage."' />
                        <p class='slide-caption' itemprop='name'>$slide->name</p></div></li>";

                }                    
               
           }

        ?>
        </ul>
    </div> 
    
    <div class="bottom-links">
        <div class="share">
            <span>SHARE</span> 
            <div class="links" >
                <div class="cell">
                <a href="https://twitter.com/share" 
                       class="twitter-share-button"
                       data-text="<?php echo $item->name ?>" 
                       data-via="massdotcom">
                    Tweet
                </a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>       
            </div>
            <div class="cell pin-it"><a href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.dgrigg.com&media=http%3A%2F%2Fwww.dgrigg.com%2Fimages%2Fprofile.jpg&description=DGrigg.com%20specializes%20in%20the%20development%20of%20interactive%20websites%20and%20web%20based%20applications.%20I%20believe%20it's%20not%20what%20you%20know%2C%20it's%20how%20fast%20can%20you%20learn%20it.%20Working%20closely%20with%20my%20clients%20demanding%20projects%20get%20done%20on%20time%20and%20on%20budget." class="pin-it-button" count-layout="horizontal"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a></div>
            <div class="cell fb-button">
                </div>
            </div>           
        </div>
        <div id="navigation" class="<?php if(strstr($_SERVER['HTTP_USER_AGENT'],'iPad')) echo 'ipad' ?>">
            <li class="prev" ></li>
            <?php
                foreach ($item->slide as $slide) {
                    echo "<li class='dots'></li>";
                }
            ?>
            <li class="next" ></li>
        </div>
        <div class="zoom">
            ZOOM
        </div>
        <div class="read">
            READ
        </div>
    </div>
</article>

