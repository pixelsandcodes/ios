function resizeElements() {
    var windowWidth = $(window).innerWidth();
    var windowHeight = $(window).innerHeight();
    var menuItems = $(".menu-item:not(.selected)");
    var mainMenu = $("#mainmenu");
    var links = $("#mainmenu li a");
    
    menuItems.each(function() {

        var size = windowWidth * $(this).attr("data-width");
        if(!$(this).hasClass("opened"))
            $(this).css({right : - size * 1.15, height : windowHeight, width : size});
        else
            $(this).css({height : windowHeight, width : windowWidth * $(this).attr("data-width")});
        
    });
   
    menuItems.css('display', 'block');
    mainMenu.css({width: $(window).innerHeight() * 0.1, "top": (windowHeight - mainMenu.outerHeight()) / 2 });
    links.css({width: $(window).innerHeight() * 0.1, height: $(window).innerHeight() * 0.1});
    // set about height to scroll
    $('.menu-item-wrapper .about').height(windowHeight * 0.92);

    $("#categories .filter").css({
      top : ($("#categories").height() - $("#categories .filter").height()) / 2
    });

    $(".menu-item-wrapper.contact").css({
      top : (windowHeight -$(".menu-item-wrapper.contact").outerHeight()) / 2
    });
    
    var contentWidth = windowHeight * 1.523809524; // iPad Landscape Width
    if(windowHeight > windowWidth){
        contentWidth = windowHeight * 0.827586207; // iPad Portrait Width
    }
    if(contentWidth > windowWidth)
        contentWidth = windowWidth; // Minimum Desktop Width

    $("#content").css({
        width : Math.round(contentWidth),
        minHeight : windowHeight + 'px',
        padding: '0 ' + (windowWidth - Math.round(contentWidth)) / 2 + 'px',
        margin : "auto"
    });
    
    $("#box-wrapper").css('width', Math.round(contentWidth * 0.9921875));
}

var interactiveBoxes, contactScroll, myPlayer;

$(document).ready(function() {
     resizeElements();

     $("#loaderImage").fadeIn(100);
     
     // Main Menu
     $("#mainmenu a").click(function(e) {
          e.preventDefault();
          $( "#search-filter" ).val('');
          var element = $(this).attr("href");

          if($('#about').hasClass("opened"))
              contactScroll.remove();

          if(element === '#about' && !$('#about').hasClass("opened"))
            setTimeout(function() {
              contactScroll = $('.menu-item-wrapper .about').niceScroll({
                cursorcolor : "#AAA",
                cursoropacitymin : 0.1,
                cursoropacitymax : 0.6,
                cursorborder : 0,
                cursorwidth : '4px',
                cursorborderradius : '2px',
                railpadding : {top: '0', bottom: '0'}
              });
            }, 600);

          if($(element).hasClass("opened")) {
              $("#mainmenu a.active").removeClass("active");
              $('.menu-item').removeClass("opened");
              $('.menu-item').stop().animate({"right" : -$(window).width() - 50 + "px"}, 600);
          } else {
              if($('.menu-item.opened').length === 0) {
                  $(element).stop().animate({"right" : 0 + "px"}, 600);
              } else {
                   $('.menu-item').stop().animate({"right" : -$(window).width() - 50 + "px"}, 600, function() {
                      $(element).stop().animate({"right" : 0 + "px"});
                   });
              }
              $('.menu-item').removeClass("opened");
              $("#mainmenu a.active").removeClass("active");
              $(element).addClass("opened");
              $(this).addClass("active");
          }


     }) ;
     
  // Map Pointer Handlers
  
});

$(window).load(function() {
   
    $("#box-wrapper").delay(300).fadeIn(600, function() {
        $("#loaderImage").fadeOut(200, function() {
            $("#mainmenu li").css('background', 'none');
            $("#mainmenu").css('right', 0);
        });
    });

    interactiveBoxes = new Boxes(document.getElementById("box-wrapper"));

    initGlobeCanvas();
 
    resizeElements();
     
    $("#mainmenu img, img.hover").hover(function () {
        var hover = $(this).attr("data-hover");
        $(this).attr("data-hover", this.src);
        this.src = hover;
    }, function () {
        var hover = $(this).attr("data-hover");
        $(this).attr("data-hover", this.src);
        this.src = hover;
    });

    $(".row.hovering").hover(function () {
        var img = $(this).find('img');
        var hover = img.attr("data-hover");
        img.attr("data-hover", img.attr("src"));
        img.attr("src", hover);
    }, function () {
        var img = $(this).find('img');
        var hover = img.attr("data-hover");
        img.attr("data-hover", img.attr("src"));
        img.attr("src", hover);
    });
    
    $("#content, .box-item").bind('click touchstart', function(e) {
        var size = window.innerWidth * $(".menu-item.opened").attr("data-width");
        try{
          contactScroll.remove();
        }
        catch(v) {
          console.log(v);
        }
        $(".menu-item.opened").animate({right : - size * 1.15}, 400, function() {
          $(this).removeClass('opened');
          $("#mainmenu li a").removeClass('active');
        });
    });
     
     var xStart, yStart = 0;
 
    document.addEventListener('touchstart',function(e) {
        xStart = e.touches[0].screenX;
        yStart = e.touches[0].screenY;
    });

    document.addEventListener('touchmove',function(e) {
        var xMovement = Math.abs(e.touches[0].screenX - xStart);
        var yMovement = Math.abs(e.touches[0].screenY - yStart);
        if((xMovement * 3) > yMovement) {
            e.preventDefault();
        }
    });
    
});

$(window).resize(function() {
  resizeElements();
});

window.onorientationchange = function()
{
  resizeElements();
};