var Gl = Gl || {}

/*
Holds a face (triangular section) of the sphere.  The 3 corners of the triangle
are demarked by the 3 shared verticies.  
*/
Gl.Face = function()
{
	this.va = new Gl.Vertex();
	this.vb = new Gl.Vertex();
	this.vc = new Gl.Vertex();
}

//Triangle points
Gl.Face.prototype.va = null;
Gl.Face.prototype.vb = null;
Gl.Face.prototype.vc = null;

//Set during rasterization so that the faces can be sorted from back to front.
Gl.Face.prototype._z = 0.0; 	

//Draws this face onto the supplied graphics object, using the supplied bitmap.
Gl.Face.prototype.Draw = function(context, img)
{
	var bdw = img.width;
	var bdh = img.height;		

	this.AffineTexTri(
				 this.va.pd.x, this.va.pd.y,
				 this.vb.pd.x, this.vb.pd.y,
				 this.vc.pd.x, this.vc.pd.y,
				 (this.va.tt.x * bdw), (this.va.tt.y * bdh),
				 (this.vb.tt.x * bdw), (this.vb.tt.y * bdh),
				 (this.vc.tt.x * bdw), (this.vc.tt.y * bdh),
				 context, img);
}


//Helper function to do an affine texture mapping.  An affine mapping is
//not 'perspective correct', but is as close as the canvas 2d renderer can get,
//and the error can be reduced by further subdividing the sphere into more faces.
//Params:
//apdx, apdy       are transformed divided coords.
//atx, aty 		   are texture coords.
Gl.Face.matA = new Gl.Matrix33A();
Gl.Face.matB = new Gl.Matrix33A();
Gl.Face.prototype.AffineTexTri = function(apdx, apdy,
								    bpdx, bpdy,
								    cpdx, cpdy,
									atx, aty,
									btx, bty,
									ctx, cty, 
									context, img)
{
	//Create a matrix which best relates the required texture mapping coords
	//to two dimensions. 
	Gl.Face.matB.a  = bpdx - apdx;
	Gl.Face.matB.b  = bpdy - apdy;
	Gl.Face.matB.c  = cpdx - apdx;
	Gl.Face.matB.d  = cpdy - apdy;
	Gl.Face.matB.tx = apdx;
	Gl.Face.matB.ty = apdy;
			
	Gl.Face.matA.a  = btx - atx;
	Gl.Face.matA.b  = bty - aty;
	Gl.Face.matA.c  = ctx - atx;
	Gl.Face.matA.d  = cty - aty;
	Gl.Face.matA.tx = atx;
	Gl.Face.matA.ty = aty;

	Gl.Face.matA.Invert();
	Gl.Face.matA.Concat(Gl.Face.matB);
	
	if( Math.abs(Gl.Face.matA.a) < 0.0001 || Math.abs(Gl.Face.matA.d) < 0.0001 )
	{
		return; //degenerate triangle, corrupts some browsers when set as a transform
	}
	
	//seems that some browsers render the triangles too small,
	//creating hairline cracks.  Enlarge the triangles slightly by factor k.
	var px = (1.0/3.0) * (apdx + bpdx + cpdx);
	var py = (1.0/3.0) * (apdy + bpdy + cpdy);
	var k = 0.15;
	
	apdx += (apdx-px) * k;
	bpdx += (bpdx-px) * k;
	cpdx += (cpdx-px) * k;
	
	apdy += (apdy-py) * k;
	bpdy += (bpdy-py) * k;
	cpdy += (cpdy-py) * k; 
	
	//Set a clip path, transform the canvas and copy the image
	//The result will be a textured triangle.
	context.save();
	
	context.beginPath();
	context.moveTo(apdx, apdy);
	context.lineTo(bpdx, bpdy);
	context.lineTo(cpdx, cpdy);
	context.lineTo(apdx, apdy);
	context.clip();

	context.transform(Gl.Face.matA.a, Gl.Face.matA.b, Gl.Face.matA.c, Gl.Face.matA.d, Gl.Face.matA.tx, Gl.Face.matA.ty);
	context.drawImage(img, 0, 0);
	
	context.restore();
	
	
}

