
var Gl = Gl || {}

//Maths utility class to hold a 3D rotation
Gl.Quat = function() {}

Gl.Quat.prototype.x = 0.0;
Gl.Quat.prototype.y = 0.0;
Gl.Quat.prototype.z = 0.0;
Gl.Quat.prototype.w = 1.0;

//Sets this quat to be equal to the supplied values
Gl.Quat.prototype.SetXYZW = function(x, y, z, w)
{
	this.x = x;
	this.y = y;
	this.z = z;
	this.w = w;
}

Gl.Quat.EPS = 1.11e-16; //epsilon
		
//Sets this quat to be equal to the supplied quat
Gl.Quat.prototype.SetAsQuat = function(q)
{
	this.x = q.x
	this.y = q.y
	this.z = q.z
	this.w = q.w
}

//If this is a unit quat, reverses the rotation
Gl.Quat.prototype.SetAsConjugate = function()
{
 	this.x = -this.x;
 	this.y = -this.y;
 	this.z = -this.z;
}
		
//Multiplys this quat by the supplied quat and places the result (combined rotation) into this quat.
Gl.Quat.prototype.Mul = function(q)
{
    var ww = this.w * q.w - this.x * q.x - this.y * q.y - this.z * q.z;
    var xx = this.w * q.x + q.w * this.x + this.y * q.z - this.z * q.y;
    var yy = this.w * q.y + q.w * this.y - this.x * q.z + this.z * q.x;
    var zz = this.w * q.z + q.w * this.z + this.x * q.y - this.y * q.x;
    this.w = ww;
    this.x = xx;
    this.y = yy;
    this.z = zz;
} 
		
//Multiplys this quat by reverse of the supplied quat and places the result in this.
Gl.Quat.tQuat1 = new Gl.Quat();
Gl.Quat.prototype.MulConjugate = function(q)
{
	var t = Gl.Quat.tQuat1;
	t.SetAsQuat(q);
	t.SetAsConjugate();
	this.Mul(t);
}
		
//Normalizes this quat to produce a unit quat.
Gl.Quat.prototype.Normalize = function()
{	
	var norm = (this.x*this.x + this.y*this.y + this.z*this.z + this.w*this.w); //Get the length squared

	if (norm > 0)
	{
		norm = 1 / Math.sqrt(norm);
		this.x *= norm;
		this.y *= norm;
		this.z *= norm;
		this.w *= norm;
	}
			
	else
	{
    	this.x = 0;
      	this.y = 0;
      	this.z = 0;
      	this.w = 0;
    }
}
		
//Sets this quat to no rotation.
Gl.Quat.prototype.SetAsIdentity = function()
{
	this.x = 0;
	this.y = 0;
	this.z = 0;
	this.w = 1;
}
		
//Sets this quat to be equivalent to the rotation described by the supplied matrix
Gl.Quat.prototype.SetAsMatrix44 = function(m)
{
	var ww = 0.25*(m.m00 + m.m11 + m.m22 + m.m33);

	if (ww >= 0)
	{
		if (ww >= Gl.Quat.EPS)
		{
	     	this.w = Math.sqrt(ww);
	    	ww =  0.25/this.w;
	    	this.x = (m.m21 - m.m12)*ww;
	       	this.y = (m.m02 - m.m20)*ww;
	       	this.z = (m.m10 - m.m01)*ww;
	       	return;
	   	} 
 	}
			
	else
	{
		this.w = 0;
		this.x = 0;
		this.y = 0;
		this.z = 1;
		return;
	}

	this.w = 0;
    ww = -0.5*(m.m11 + m.m22);
       
    if (ww >= 0)
	{
		if (ww >= Gl.Quat.EPS)
		{
	    	this.x = Math.sqrt(ww);
	       	ww = 1/(2*this.x);
	       	this.y = m.m10*ww;
	       	this.z = m.m20*ww;
	       	return;
	   	}
	}
			
	else 
	{
		this.x = 0;
	   	this.y = 0;
	   	this.z = 1;
	   	return;
    }
     
	this.x = 0;
    ww = 0.5*(1.0 - m.m22);

    if (ww >= Gl.Quat.EPS)
	{
		this.y = Math.sqrt(ww);
	   	this.z = m.m21/(2*this.y);
	   	return;
	}
     
    this.y = 0;
    this.z = 1;
}
   		
//Sets this quat to be equivalent to the amount of rotation (angle) around the supplied axis
Gl.Quat.prototype.SetFromAxisAndAngle = function(ax, ay, az, angle)
{	
	var amag = Math.sqrt( ax * ax + ay * ay + az * az);
		
	if (amag < Gl.Quat.EPS )
	{
		this.w = 0;
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}
			
	else
	{  
		amag = 1/amag; 
		var mag = Math.sin(angle/2.0);
		this.w = Math.cos(angle/2.0);
		this.x = ax * amag * mag;
		this.y = ay * amag * mag;
	    this.z = az * amag * mag;
	}
}
		
//Sets this quat to be the spherical interpolation at alpha between itself, and the supplied quat
Gl.Quat.prototype.SetAsInterpolate = function(q, alpha)
{ 
	var s1;
	var s2;
	var om;
	var sinom;

    var dp = this.x * q.x + this.y * q.y + this.z * q.z + this.w * q.w;

    if ( dp < 0 )
	{
    	//Negate the quat
       	q.x = -q.x;
       	q.y = -q.y;
       	q.z = -q.z;
       	q.w = -q.w;
       	dp = -dp;
	}

	if ( (1.0 - dp) > Gl.Quat.EPS )
	{
		om = Math.acos(dp);
       	sinom = Math.sin(om);
       	s1 = Math.sin((1.0-alpha)*om)/sinom;
       	s2 = Math.sin( alpha*om)/sinom;
    }
			
	else
	{
    	s1 = 1.0 - alpha;
       	s2 = alpha;
	}

    this.w = (s1 * this.w + s2 * q.w);
    this.x = (s1 * this.x + s2 * q.x);
    this.y = (s1 * this.y + s2 * q.y);
    this.z = (s1 * this.z + s2 * q.z);
}
  		
//Transforms the supplied direction (v) by the rotation described by this quat.
Gl.Quat.tMatrix1 = new Gl.Matrix44();
Gl.Quat.prototype.Transform = function(v)
{
	var m = Globe.Quat.tMatrix1;
	m.SetAsQuat(this);
	m.TransformDirection(v);
}
		
//Transforms the supplied direction (v) by the opposite of the rotation descibed by this quat.
Gl.Quat.tQuat2 = new Gl.Quat();
Gl.Quat.prototype.TransformConjugate = function(v)
{
	var q = Globe.Quat.tQuat2;
	q.SetAsQuat(this);
	q.SetAsConjugate();
	q.Transform(v);
}
				
//Scales the supplied vector and adds the result to this quat.
Gl.Quat.tQuat3 = new Gl.Quat();
Gl.Quat.prototype.AddScaledVector = function(vector, scale)
{
	var q = Gl.Quat.tQuat3;
    q.SetXYZW(vector.x * scale, vector.y * scale, vector.z * scale, 0);
        	
    q.Mul(this);
            
    this.w += q.w * 0.5;
    this.x += q.x * 0.5;
    this.y += q.y * 0.5;
    this.z += q.z * 0.5;
                        
    this.Normalize();
            
    if( isNaN(this.w) || isNaN(this.x) || isNaN(this.y) || isNaN(this.z) )
    	throw""; 
}

//Sets this quat to point at a position, using a temporary matrix
Gl.Quat.t0Matrix44 = new Gl.Matrix44();
Gl.Quat.tVec1 = new Gl.Vector3();
Gl.Quat.prototype.SetAsLookAt = function(lookAt, up)
{
	Gl.Quat.tVec1.SetZero();
	Gl.Quat.t0Matrix44.SetAsPointAt(Gl.Quat.tVec1, lookAt, up);
	this.SetAsMatrix44(Gl.Quat.t0Matrix44);
}	


//Sets this matrix to be equivalent to the rotation represented by the supplied quat
Gl.Quat.prototype.GetMatrix44 = function(m)
{
	m.m00 = (1.0 - 2.0 * this.y * this.y - 2.0 * this.z * this.z);
	m.m10 = (2.0 * (this.x * this.y + this.w * this.z));
	m.m20 = (2.0 * (this.x * this.z - this.w * this.y));
	m.m30 = 0;
					
	m.m01 = (2.0 * (this.x * this.y - this.w * this.z));
	m.m11 = (1.0 - 2.0 * this.x * this.x - 2.0 * this.z * this.z);
	m.m21 = (2.0 * (this.y * this.z + this.w * this.x));
	m.m31 = 0;
				
	m.m02 = (2.0 * (this.x * this.z + this.w * this.y));
	m.m12 = (2.0 * (this.y * this.z - this.w * this.x));
	m.m22 = (1.0 - 2.0 * this.x * this.x - 2.0 * this.y * this.y);
	m.m32 = 0;
					
	m.m03 = 0;
	m.m13 = 0;
	m.m23 = 0;
	m.m33 = 1;
}


