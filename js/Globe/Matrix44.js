
var Gl = Gl || {}

//Utility math class to represent a 4x4 matrix
Gl.Matrix44 = function() {}

//Matrix elements (4x4 = 16).  Row major: m23 is row 2, col 3
Gl.Matrix44.prototype.m00 = 1.0;
Gl.Matrix44.prototype.m01 = 0.0;
Gl.Matrix44.prototype.m02 = 0.0;
Gl.Matrix44.prototype.m03 = 0.0;
Gl.Matrix44.prototype.m10 = 0.0;
Gl.Matrix44.prototype.m11 = 1.0;
Gl.Matrix44.prototype.m12 = 0.0;
Gl.Matrix44.prototype.m13 = 0.0;
Gl.Matrix44.prototype.m20 = 0.0;
Gl.Matrix44.prototype.m21 = 0.0;
Gl.Matrix44.prototype.m22 = 1.0;
Gl.Matrix44.prototype.m23 = 0.0;
Gl.Matrix44.prototype.m30 = 0.0;
Gl.Matrix44.prototype.m31 = 0.0;
Gl.Matrix44.prototype.m32 = 0.0;
Gl.Matrix44.prototype.m33 = 1.0;

//Sets this matrix to be equal to the supplied matrix
Gl.Matrix44.prototype.SetAsMatrix44 = function(m)
{
	this.m00 = m.m00;
	this.m01 = m.m01;
	this.m02 = m.m02;
	this.m03 = m.m03;

	this.m10 = m.m10;
	this.m11 = m.m11;
	this.m12 = m.m12;
	this.m13 = m.m13;

	this.m20 = m.m20;
	this.m21 = m.m21;
	this.m22 = m.m22;
	this.m23 = m.m23;

	this.m30 = m.m30;
	this.m31 = m.m31;
	this.m32 = m.m32;
	this.m33 = m.m33;
}
		 		
//Sets this matrix to zero rotation and zero positional change
Gl.Matrix44.prototype.SetAsIdentity = function()
{
	this.m00 = 1;
	this.m01 = 0;
	this.m02 = 0;
	this.m03 = 0;

	this.m10 = 0;
	this.m11 = 1;
	this.m12 = 0;
	this.m13 = 0;

	this.m20 = 0;
	this.m21 = 0;
	this.m22 = 1;
	this.m23 = 0;

	this.m30 = 0;
	this.m31 = 0;
	this.m32 = 0;
	this.m33 = 1;
}

//Sets this matrix to the given scale.
Gl.Matrix44.prototype.SetAsScale = function(x, y, z)
{
	this.SetAsIdentity();
	this.m00 = x;
	this.m11 = y;			
	this.m22 = z;
}

//Sets this matrix to scale and translate device coords to the given screen coords
Gl.Matrix44.prototype.SetAsViewport = function(l, r, t, b)
{
	var k = t;
	t = b;
	b = k;
					
	this.m00 = (r - l)/2;
	this.m10 = 0;
	this.m20 = 0;
	this.m30 = 0;
					
	this.m01 = 0;
	this.m11 = (b - t) / 2;
	this.m21 = 0;
	this.m31 = 0;
				
	this.m02 = 0;
	this.m12 = 0;
	this.m22 = 1;
	this.m32 = 0;
					
	this.m03 = (l + r)/2
	this.m13 = (t + b)/2
	this.m23 = 0;
	this.m33 = 1;
}
				
//Sets this matrix to transform by the given translation (position in space)
Gl.Matrix44.prototype.SetAsTranslation = function(x, y, z)
{
	this.m00 = 1;
	this.m01 = 0;
	this.m02 = 0;
	this.m03 = x; 

	this.m10 = 0;
	this.m11 = 1;
	this.m12 = 0;
	this.m13 = y;

	this.m20 = 0;
	this.m21 = 0;
	this.m22 = 1;
	this.m23 = z;

	this.m30 = 0;
	this.m31 = 0;
	this.m32 = 0;
	this.m33 = 1;		
}
				
//Sets this matrix to the result of multiplying itself by m
Gl.Matrix44.prototype.Mul = function(m1)
{
	var m00 = this.m00*m1.m00 + this.m01*m1.m10 + this.m02*m1.m20 + this.m03*m1.m30;
	var m01 = this.m00*m1.m01 + this.m01*m1.m11 + this.m02*m1.m21 + this.m03*m1.m31;
	var m02 = this.m00*m1.m02 + this.m01*m1.m12 + this.m02*m1.m22 + this.m03*m1.m32;
	var m03 = this.m00*m1.m03 + this.m01*m1.m13 + this.m02*m1.m23 + this.m03*m1.m33;

	var m10 = this.m10*m1.m00 + this.m11*m1.m10 + this.m12*m1.m20 + this.m13*m1.m30; 
	var m11 = this.m10*m1.m01 + this.m11*m1.m11 + this.m12*m1.m21 + this.m13*m1.m31;
	var m12 = this.m10*m1.m02 + this.m11*m1.m12 + this.m12*m1.m22 + this.m13*m1.m32;
	var m13 = this.m10*m1.m03 + this.m11*m1.m13 + this.m12*m1.m23 + this.m13*m1.m33;

	var m20 = this.m20*m1.m00 + this.m21*m1.m10 + this.m22*m1.m20 + this.m23*m1.m30; 
	var m21 = this.m20*m1.m01 + this.m21*m1.m11 + this.m22*m1.m21 + this.m23*m1.m31;
	var m22 = this.m20*m1.m02 + this.m21*m1.m12 + this.m22*m1.m22 + this.m23*m1.m32;
	var m23 = this.m20*m1.m03 + this.m21*m1.m13 + this.m22*m1.m23 + this.m23*m1.m33;

	var m30 = this.m30*m1.m00 + this.m31*m1.m10 + this.m32*m1.m20 + this.m33*m1.m30; 
	var m31 = this.m30*m1.m01 + this.m31*m1.m11 + this.m32*m1.m21 + this.m33*m1.m31;
	var m32 = this.m30*m1.m02 + this.m31*m1.m12 + this.m32*m1.m22 + this.m33*m1.m32;
	var m33 = this.m30*m1.m03 + this.m31*m1.m13 + this.m32*m1.m23 + this.m33*m1.m33;
		 
	this.m00 = m00; this.m01 = m01; this.m02 = m02; this.m03 = m03;
	this.m10 = m10; this.m11 = m11; this.m12 = m12; this.m13 = m13;
	this.m20 = m20; this.m21 = m21; this.m22 = m22; this.m23 = m23;
	this.m30 = m30; this.m31 = m31; this.m32 = m32; this.m33 = m33;
}
				
//Transforms the given point by this matrix, in this case setting z to the value of w
Gl.Matrix44.prototype.TransformPointPerspective = function(point)
{
	var x = this.m00*point.x + this.m01*point.y + this.m02*point.z + this.m03;
	var y = this.m10*point.x + this.m11*point.y + this.m12*point.z + this.m13;
	var z = this.m20*point.x + this.m21*point.y + this.m22*point.z + this.m23;
	var w = this.m30*point.x + this.m31*point.y + this.m32*point.z + this.m33;
	point.x = x;
	point.y = y;
	point.z = w;
}
				
//Transforms the given point by this matrix, assuming missing w value is 1
Gl.Matrix44.prototype.TransformPoint = function(point)
{
	var x = this.m00*point.x + this.m01*point.y + this.m02*point.z + this.m03;
	var y = this.m10*point.x + this.m11*point.y + this.m12*point.z + this.m13;
	var z = this.m20*point.x + this.m21*point.y + this.m22*point.z + this.m23;
	point.x = x;
	point.y = y;
	point.z = z;
}
			
//Transforms the given direction by this matrix
Gl.Matrix44.prototype.TransformDirection = function(vec)
{
	var x = this.m00*vec.x + this.m01*vec.y + this.m02*vec.z;
	var y = this.m10*vec.x + this.m11*vec.y + this.m12*vec.z;
	var z = this.m20*vec.x + this.m21*vec.y + this.m22*vec.z;
	vec.x = x;
	vec.y = y;
	vec.z = z;
}
							
//Transforms the given vector by the inverse of this matrix, relying on the
//matrix being free of shear, scale, and perspective changes.
Gl.Matrix44.prototype.TransformInversePoint = function(vector)
{
	var xx = vector.x - this.m03;
	var yy = vector.y - this.m13;
	var zz = vector.z - this.m23;
		        	
	var x = xx * this.m00 + yy * this.m10 + zz * this.m20;
	var y = xx * this.m01 + yy * this.m11 + zz * this.m21;
	var z = xx * this.m02 + yy * this.m12 + zz * this.m22;
					
	vector.x = x;
	vector.y = y;
	vector.z = z;
}
				
//Sets this matrix as a perspective change, given the supplied field of view, aspect, and near and far planes
//Mathematics for 3d game programming and compuer graphics p124
Gl.Matrix44.prototype.SetAsPerspective = function(fov, aspect, n, f)
{
	var e = 1 / Math.tan(fov/2);
					
	this.m00 = e;
	this.m10 = 0;
	this.m20 = 0;
	this.m30 = 0;
					
	this.m01 = 0;
	this.m11 = e/aspect;
	this.m21 = 0;
	this.m31 = 0;
					
	this.m02 = 0; 
	this.m12 = 0;
	this.m22 = -(f + n) / (f - n);
	this.m32 = -1;
					
	this.m03 = 0;
	this.m13 = 0;
	this.m23 = -( (2 * f * n) / ( f - n) );
	this.m33 =  0;
}

//Sets this matrix to be the equivalent rotation of being at point 'eye', looking
//at point 'center', with regard to 'up' (normally 0, 1, 0)
Gl.Matrix44.t0Vector3 = new Gl.Vector3();
Gl.Matrix44.t1Vector3 = new Gl.Vector3();
Gl.Matrix44.t2Vector3 = new Gl.Vector3();
Gl.Matrix44.prototype.SetAsPointAt = function(eye, center, up)
{
	var f = Gl.Matrix44.t0Vector3;
	var u = Gl.Matrix44.t1Vector3;
	var s = Gl.Matrix44.t2Vector3;
	
	f.SetXYZ(center.x - eye.x, center.y - eye.y, center.z - eye.z); 
	u.SetXYZ(up.x, up.y, up.z);
	f.Normalize();
	s.SetCross(f, u);
	s.Normalize();			
	u.SetCross(s, f);
	
	this.m00 =  s.x; this.m10 =  s.y; this.m20 =   s.z; this.m30 = 0;
	this.m01 =  u.x; this.m11 =  u.y; this.m21 =   u.z; this.m31 = 0;
	this.m02 = -f.x; this.m12 = -f.y; this.m22 =  -f.z; this.m32 = 0;
	this.m03 =    0; this.m13 =    0; this.m23 =   0.0; this.m33 = 1;
}
				

	
