var Gl = Gl || {}

//Utility class to average out the passage of 'frames', or redraw callbacks
//in order to give a reasonable estimate of the time elapsed since the last frame
Gl.Clock = function(n, dmin, dmax)
{
	n 	 =    n || 10;
	dmin = dmin || 0.001;
	dmax = dmax || 1.0 / 5.0;
	
	this.dmin = dmin;
	this.dmax = dmax;
	this.t    = this.Time();
	this.d    = dmax;
	this.i    = 0;
	this.arr  = [];
	
	for(var a = 0 ; a < n ; a++)
		this.arr[a] = dmax;
}

Gl.Clock.prototype.dmin = 0.0; //Clamp frame delta to largest and smallest times
Gl.Clock.prototype.dmax = 0.0;
Gl.Clock.prototype.t = 0.0;    //time of the last frame
Gl.Clock.prototype.d = 0.0;    //current delta time value
Gl.Clock.prototype.i = 0;      //position in circular buffer
Gl.Clock.prototype.arr = null; //circular buffer of frame deltas

//Get the system time in seconds
Gl.Clock.prototype.Time = function()
{
	return new Date().getTime() / 1000.0;
}

//Call once per frame to update the frame 'delta' value
Gl.Clock.prototype.NextFrame = function()
{
	var time = this.Time();
	this.arr[this.i] = time - this.t;
	this.t = time;
	
	this.i++;
	if( this.i == this.arr.length )
		this.i = 0;
	
	var sum = 0.0;
	for(var a = 0 ; a < this.arr.length ; a++)
		sum += this.arr[a];
		
	this.d = sum / (this.arr.length);
	
	     if( this.d < this.dmin ) this.d = this.dmin;
	else if( this.d > this.dmax ) this.d = this.dmax;
}

//Get the pre-computed delta value for the current frame
Gl.Clock.prototype.Delta = function()
{
	return this.d;
}


