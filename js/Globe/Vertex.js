
var Gl = Gl || {}

//Holds the information for a single 3D point.
Gl.Vertex = function()
{
	this.pu = new Gl.Vector3();
	this.pd = new Gl.Vector3();
	this.tt = new Gl.Vector2();
}

Gl.Vertex.prototype.pu = null; //point untransformed (model coords)
Gl.Vertex.prototype.pd = null; //point transformed and divided (screen coords)
Gl.Vertex.prototype.tt = null; //texture coords.

	