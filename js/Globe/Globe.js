var Gl = Gl || {}

//Creates a representation of a 3d sphere, texture mapped with the supplied bitmap
Gl.Globe = function(canvas)
{
	this.arrRenderList = [];
	this.arrVerticies  = [];
	this.arrFaces 	   = [];
	
	this.canvas  = canvas;
	this.context = canvas.getContext('2d');
	this.quat   = new Gl.Quat();
	this.mCam   = new Gl.Matrix44(); 
	this.mGlobe = new Gl.Matrix44();
	this.mFrame = new Gl.Matrix44(); 
	this.angVel = new Gl.Vector3();
	
	this.quatRotateFrom = new Gl.Quat();
	this.quatRotateTo = new Gl.Quat();
	
	this.dragStartPoint = new Gl.Vector3();
	this.dragStartAng   = new Gl.Quat(); 
	
	//Set angular position to no rotation
	this.mGlobe.SetAsIdentity();
	this.quat.SetAsIdentity();
	
	//Set up initial matrix values
	this.RecalcCameraTransform();
	this.RecalcGlobeMatrix();
	
	this.SetDetail(20); //Approximate sphere geometry, subdividing into given number of points across and down
	
	
	
	this.dragStartPoint.x = NaN;	//.x of NaN represents no dragging currently
	
	this.angVel.SetXYZ(0, 0, 0); //Initially set to no rotation
	
	this.clock = new Gl.Clock();
	
	//hook up the events we need to know about.  Convoluted torture.
	var self = this;
	
	if (typeof(window.ontouchstart) != 'undefined')
		$(canvas).on("touchstart", self, Gl.Globe.OnCanvasTouchStart);
	
	else
		$(canvas).on("mousedown", self, Gl.Globe.OnCanvasMouseDown);
		
	setInterval(function(){self.Draw();}, 0.0);
	
	this.RequireDraw();
}


//set up some constants that anchor the sphere centre to the world centre
//and place the camera at a fixed point some distance away.
Gl.Globe.GEOM_RADIUS 	= 5.0;		//radius of the sphere
Gl.Globe.CAM_Z 			= 13;		//distance from world centre of the camera.
Gl.Globe.CAM_NEAR_PLANE = 1;		//camera near plane
Gl.Globe.CAM_FAR_PLANE 	= 1000;		//camera far plane
Gl.Globe.CAM_FOV 		= Math.PI/4;	//field of view of the camera
		
Gl.Globe.prototype.arrRenderList = null;	//Temp List of triangles to be drawn in the current frame
Gl.Globe.prototype.arrVerticies  = null; //Const list of verticies (3d points) that compose the sphere
Gl.Globe.prototype.arrFaces 	 = null; //Const list of faces (composed of verticies) that create the sphere
Gl.Globe.prototype.img 			 = null;	//Texture to wrap around the sphere
Gl.Globe.prototype.quat 		 = null; //The current angular position of the sphere
Gl.Globe.prototype.mCam 		 = null; //Matrix to represent the camera transform
Gl.Globe.prototype.mGlobe 		 = null; //Matrix to represent the sphere transform (angular position)
Gl.Globe.prototype.mFrame 		 = null; //Matrix to hold the current combination of the two above matricies.
Gl.Globe.prototype.cullBackFaces = true; //If true, do not draw faces which are pointing away from us (to save time)
Gl.Globe.prototype.angVel 		 = null; //Angular velocity (rate of change of angular position) of the sphere.  (Speed at which it turns)
Gl.Globe.prototype.allowDrag 	 = true; //If true, allow the user to drag the sphere with the mouse.
Gl.Globe.prototype.canvas		 = null; //the passed in canvas DOM element
Gl.Globe.prototype.context 		 = null; //the context taken from the above canvas.
Gl.Globe.prototype.canvasMouseX  = 0; //Track the mouse position
Gl.Globe.prototype.canvasMouseY  = 0;
//Gl.Globe.prototype.timeout  	 = undefined; //Store the timeout for the drawing callback
Gl.Globe.prototype.damping 		 = 0.3; //Adds 'weight' to the globe by slowing it down.  1 = off.

Gl.Globe.prototype.clock		 = null;

Gl.Globe.prototype.requireDraw   = false;

//Called when the globe should be redrawn.  Calls are accumulated into one.
Gl.Globe.prototype.RequireDraw = function()
{
	this.requireDraw = true; 
}

//Return the last known mouse point
Gl.Globe.prototype.GetMousePoint = function()
{
	var v = new Gl.Vector2();
	v.x = this.canvasMouseX;
	v.y = this.canvasMouseY;
	return v;
}

Gl.Globe.prototype.GetOffsetLeft = function(e)
{
	var left = 0;
	getOffsetParentLeft(e.data.canvas);

	function getOffsetParentLeft(element) {
		left += element.offsetLeft;
		if(element.offsetParent instanceof HTMLElement)
			return getOffsetParentLeft(element.offsetParent);
		else return;
	}

	return left;
};

Gl.Globe.prototype.GetOffsetTop = function(e)
{
	var top = 0;
	getOffsetParentLeft(e.data.canvas);

	function getOffsetParentLeft(element) {
		top += element.offsetTop;
		if(element.offsetParent instanceof HTMLElement)
			return getOffsetParentLeft(element.offsetParent);
		else return;
	}

	return top;
};

//Set how much the globe should slow down as it spins.  0= stop, 1=no damping
Gl.Globe.prototype.SetDamping = function(d)
{
	this.damping = d;
}

//These vars are the system that allows us to rotate smoothly to a given point
//using the RotateTo function.  From and to hold the start and end points 
//of the interpolation, at a rate controlled by the speed.
Gl.Globe.prototype.quatRotateFrom = null;
Gl.Globe.prototype.quatRotateTo = null;
Gl.Globe.prototype.rotateToSpeed = -1;
Gl.Globe.prototype.rotateToPosition = -1;		

Gl.Globe.prototype.dragStartPoint = null; //Local point on sphere where user mouses down
Gl.Globe.prototype.dragStartAng   = null; //Current rotation of the sphere when the user mouses down

//Set the texture to be wrapped around the sphere.
//does not dispose the previous image.
Gl.Globe.prototype.SetImage = function(img)
{
	this.img = img;
	this.newImage = true;
	this.RequireDraw();
}
		
	
//Sets the angular velocity (speed of rotation) of the sphere around the 3
//axis in rads per frame.  An angular vel of (0, 0.02, 0) for example, will
//keep the north and south poles centered and spin around the equator.
Gl.Globe.prototype.SetAngularVelocity = function(x, y, z)
{
	this.angVel.SetXYZ(x, y, z);
	this.RequireDraw();
}

Gl.Globe.prototype.GetAngularVelocity = function()
{
	var v = new Gl.Vector3();
	v.SetVector3(this.angVel);
	return v;
}

//Sets whether the user can drag points on the surface of the globe
Gl.Globe.prototype.SetAllowDrag = function(d)
{
	this.allowDrag = d;
}

Gl.Globe.OnBrowserTouchMove = function(e)
{
	e.preventDefault();
	
	var self = e.data;
	
	var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
	
	self.canvasMouseX = touch.pageX - self.GetOffsetLeft(e);
	self.canvasMouseY = touch.pageY - self.GetOffsetTop(e);
	self.RequireDraw();	
}

//Called by the framework when the mouse moves in the browser window
Gl.Globe.OnBrowserMouseMove = function(e)
{
	var self = e.data;
	self.canvasMouseX = e.pageX - self.GetOffsetLeft(e);
	self.canvasMouseY = e.pageY - self.GetOffsetTop(e);
	self.RequireDraw();	
}

Gl.Globe.OnCanvasTouchStart = function(e)
{
	var self = e.data;
	
	if( self.allowDrag )
	{
		var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
		
		self.canvasMouseX = touch.pageX - self.GetOffsetLeft(e);
		self.canvasMouseY = touch.pageY - self.GetOffsetTop(e);
		
		self.dragStartAng.SetAsQuat(self.quat);
		self.PixelToPoint(self.canvasMouseX, self.canvasMouseY, self.dragStartPoint);
		self.mGlobe.TransformPoint(self.dragStartPoint);
		
		//We only need these handlers when we're dragging - keep them turned off at other times
		$(document).on("touchend",   self, Gl.Globe.OnBrowserTouchEnd);
		$(document).on("touchmove", self, Gl.Globe.OnBrowserTouchMove);
	}	
}

//If we are allowing the user to drag the globe, note the point clicked and the current rotation
Gl.Globe.OnCanvasMouseDown = function(e)
{
	var self = e.data;
	
	if( self.allowDrag )
	{
		self.canvasMouseX = e.pageX - self.GetOffsetLeft(e);
		self.canvasMouseY = e.pageY - self.GetOffsetTop(e);
		
		self.dragStartAng.SetAsQuat(self.quat);
		self.PixelToPoint(self.canvasMouseX, self.canvasMouseY, self.dragStartPoint);
		self.mGlobe.TransformPoint(self.dragStartPoint);
		
		//We only need these handlers when we're dragging - keep them turned off at other times
		$(document).on("mouseup",   self, Gl.Globe.OnBrowserMouseUp);
		$(document).on("mousemove", self, Gl.Globe.OnBrowserMouseMove);
	}	
}

Gl.Globe.OnBrowserTouchEnd = function(e)
{
	var self = e.data;
	
	self.dragStartPoint.x = NaN;
	
	$(document).off("touchend",   Gl.Globe.OnBrowserTouchEnd);
	$(document).off("touchmove", Gl.Globe.OnBrowserTouchMove);
}
		
//When the mouse button goes up, signal the end of the dragging.
Gl.Globe.OnBrowserMouseUp = function(e)
{
	var self = e.data;
	
	self.dragStartPoint.x = NaN;
	
	$(document).off("touchend mouseup",   Gl.Globe.OnBrowserMouseUp);
	$(document).off("touchmove mousemove", Gl.Globe.OnBrowserMouseMove);
}
		
//Sets whether faces facing away from the camera should not be drawn
//If you use a bitmap with a transparency channel, when this is set to false
//you can see through the globe to the opposing side
Gl.Globe.prototype.SetCullBackFaces = function(c)
{
	this.cullBackFaces = c;
	this.RequireDraw();
}
		
//Simply return the canvas we are drawing on
Gl.Globe.prototype.GetCanvas = function() { return this.canvas; }
		
//Helper function to translate from a given lattitude and longitude, to a 3d
//point in model coordinates.
Gl.Globe.prototype.LatLngToPoint = function(lat, lng, radius, out)
{
	//get lat from current scale, to -Pi/2 to +Pi/2
	lat += 90;
	lat /= 180;
			
	//get lng from current scale, to -Pi to +Pi 
	lng += 180;
	lng /= 360;
			
	lat *= (Math.PI);
	lng *= (Math.PI * 2);
			
	//Source: http://en.wikipedia.org/wiki/Spherical_coordinate
	out.SetXYZ(radius * Math.sin(lat) * Math.sin(lng),
	-radius * Math.cos(lat),
	radius * Math.sin(lat) * Math.cos(lng)
			
	);
		 
}
		
//Sets the level of detail of the representation of the sphere.  Higher detail
//looks better, but increases the work and slows the animation down.  Reasonable
//values range from about 10 to 30 but can be any positive integer.
Gl.Globe.prototype.SetDetail = function(n)
{
	//Clear the geometry we currently have (if any)
	this.arrVerticies.length = 0;
	this.arrFaces.length 	= 0;
		
	var v;
	var f1;
	var f2;
	var x;
	var y;
			
	//'wrap' the sphere in 3d points from the north to south pole, and ranging around the sphere
	for(y = 0 ; y < n ; y++ )
	{
		for(x = 0 ; x < n ; x++)
		{
			var lat = +90  - ( (y / (n-1)) * 180);
			var lng = -180 + ( (x / (n-1)) * 360);
					
			//Create a vertex at that lat:lng using the helper function
			v = new Gl.Vertex();
					
			this.LatLngToPoint(lat, lng, Gl.Globe.GEOM_RADIUS, v.pu);
					
			//Vertex also needs a texture mapping coordinate, ranging from 0->1:0->1
			var s = Number(x) / Number(n-1);
			var t = Number(y) / Number(n-1);
					
			v.tt.x = s;
			v.tt.y = t;
					
			this.arrVerticies.push(v);
		}
	}
			
	//Group verticies into triangles.  Triangles will share their points
	//with other triangles.  Do this by stepping over the verticies to create
	//rectangles, then dividing each rect into two diagonally to form two triangles
	for(y = 0 ; y < n-1 ; y++)
	{
		for(x = 0 ; x < n-1 ; x++)
		{
			f1 = new Gl.Face();
			f1.va = this.arrVerticies[x + (y*n)];
			f1.vb = this.arrVerticies[x + (y*n) + 1];
			f1.vc = this.arrVerticies[x + ((y+1)*n)];
			this.arrFaces.push(f1);
					
			f2 = new Gl.Face();
			f2.va = this.arrVerticies[x + (y*n) + 1];
			f2.vb = this.arrVerticies[x + ((y+1)* n) + 1];
			f2.vc = this.arrVerticies[x + ((y+1)* n)];
			this.arrFaces.push(f2);
		}
	}
	
	this.RequireDraw();
}
		
//Helper function to convert the given pixel (in coordinates of local sprite)
//to a 3d point local to the model.  This is probably the toughest function here.
//We need to project a ray out of the viewing position into the 3d world, and
//find where it would intersect on the sphere.
//source: http://www.siggraph.org/education/materials/HyperGraph/raytrace/rtinter1.htm
Gl.Globe.tVec8 = new Gl.Vector3();
Gl.Globe.tVec9 = new Gl.Vector3();
Gl.Globe.prototype.PixelToPoint = function(x, y, out)
{
	//we know from the constants we initially set up that the sphere
	//fills the space indicated by the canvas size.  So we can get those
	//coords in the space -1 -> 1
	x /= this.canvas.width/2;
	y /= this.canvas.height/2; /// div 2?
	x -= 1.0;
	y -= 1.0;
			
	//Set a point where the camera is
	var pos = Gl.Globe.tVec8;
	pos.SetXYZ(0, 0, -Gl.Globe.CAM_Z);
			
	//Now get a point on a plane passing through the sphere
	var dir = Gl.Globe.tVec9;
	dir.SetXYZ(-x*(Gl.Globe.GEOM_RADIUS), y*(Gl.Globe.GEOM_RADIUS), -Gl.Globe.CAM_NEAR_PLANE);
			
	//Subtract the second point from the first and normalize, to get a direction
	dir.Sub(pos);
	dir.Normalize();
			
	//See link above
	var a = (dir.x * dir.x) + (dir.y*dir.y) + (dir.z * dir.z);
	var b = 2 * (dir.x * pos.x + dir.y * pos.y + dir.z * pos.z);
	var c = (pos.x*pos.x) + (pos.y*pos.y) + (pos.z*pos.z) - (Gl.Globe.GEOM_RADIUS*Gl.Globe.GEOM_RADIUS);
			
	var t0 = (-b - Math.sqrt((b*b) - 4 * a *c)) / 2*a;
			
	var dist = t0;
		
	//the ray could well miss the sphere altogether:
	if( dist < 0 || isNaN(dist) ) { out.x = NaN; out.y = NaN; return; }
			
	//if not, dist represents how far to travel along that ray until you 
	//get to the sphere intersection point.  So start at the camera
	//and travel that distance.
	dir.Mul(dist);
	dir.Add(pos);
			
	//Finally the globe could be rotated, so take off that rotation
	this.mGlobe.TransformInversePoint(dir);
			
	out.SetVector3(dir);
}
		
//Helper function to transform a pixel (local sprite coords to a lat/lng pair
//Do this by finding the model point, then converting to spherical coords,
//then transforming those coords into the friendly range.
Gl.Globe.tVec7 = new Gl.Vector3();
Gl.Globe.prototype.PixelToLatLng = function(x, y, out)
{				
	var dir = Gl.Globe.tVec7;
	this.PixelToPoint(x, y, dir);
			
	//Our model is not the same way up as the standard text
	var xx = dir.z;
	var yy = dir.x;
	var zz = dir.y;
			
	//Source http://en.wikipedia.org/wiki/Spherical_coordinates
	var p = Math.sqrt( (xx*xx)+(yy*yy)+(zz*zz) );
			
	var theta = Math.acos(zz / p);
	var phi   = Math.atan2(yy, xx);
			
	//We want lat measured in -90 to +90 and lng -180 to +180
	theta /= Math.PI;
	theta *= 180;
	theta -= 90;
			
	phi /= Math.PI;
	phi *= 180;
			
	//Make sure our coords wrap around correctly.
	if( theta < -90 ) theta += 180;
	if( theta > +90 ) theta -= 180;
	if( phi   < -180 ) phi += 360;
	if( phi   > +180 ) phi -= 360;
			
	out.x = theta;
	out.y = phi;
}
				
//Helper function to convert from lat/lng pair to a pixel (local sprite coords)
//We can achieve this easily by finding the 3d point, and transforming it
//by the current combined projection matrix.
Gl.Globe.tVec6 = new Gl.Vector3();
Gl.Globe.prototype.LatLngToPixel = function(lat, lng, radius, out)
{
	var v = Gl.Globe.tVec6;
	this.LatLngToPoint(lat, lng, radius, v);
			
	//mFrame holds the combined matrix for this frame
	this.mFrame.TransformPointPerspective(v);
			
	//perspective divide.
	out.x = v.x / v.z;
	out.y = v.y / v.z;
				
	return -v.z + Gl.Globe.CAM_Z;
}
		

//Smoothly interpolate to the position such that the given lat/lng pair
//is directly in front of the camera (centered).  A speed of 1 would be an 
//instant transition.
Gl.Globe.tVec4 = new Gl.Vector3();
Gl.Globe.tVec5 = new Gl.Vector3();
Gl.Globe.tVec10 = new Gl.Vector3();
Gl.Globe.prototype.RotateTo = function(lat, lng, speed) 
{
	speed = speed || 1.0;
	//Get a point describing the camera position
	var eye = Gl.Globe.tVec4;
	eye.SetXYZ(0, 0, Gl.Globe.CAM_Z);
			
	//Find the point describing the position we want to work with
	var target = Gl.Globe.tVec5;
	this.LatLngToPoint(lat, lng, Gl.Globe.GEOM_RADIUS, target);
			
	//Rotate from current ang, to the ang that would bring the point into line
	this.quatRotateFrom.SetAsQuat(this.quat);
	//quatRotateTo.SetAsAngleBetween(target, eye);
			
	var up = Gl.Globe.tVec10;
	up.SetXYZ(0, 1, 0);
			
	target.Mul(-1);
	this.quatRotateTo.SetAsLookAt(target, up);
	this.quatRotateTo.SetAsConjugate();
				
	//Reset the position to zero and prepare the speed
	this.rotateToSpeed = speed;	
	this.rotateToPosition = 0;
	
	this.RequireDraw();
}
		
//For the current frame, assuming that we are smoothly rotating to the 
//position specified in RotateTo(), modify the angle of the globe by an 
//amount dependant on the speed
Gl.Globe.prototype.DoRotate = function(dt)
{
	//Add the speed to the position, don't go past 1 or we will actually
	//travel further than required.
	this.rotateToPosition += this.rotateToSpeed * dt;
	if( this.rotateToPosition > 1 ) this.rotateToPosition = 1;
			
	//Lerp from start to end quat (a major advantage of using quats to represent rotation)
	this.quat.SetAsQuat(this.quatRotateFrom);
	this.quat.SetAsInterpolate(this.quatRotateTo, this.rotateToPosition);
	this.RecalcGlobeMatrix();
			
	//If that was the last part of the journey, turn off rotation
	if( this.rotateToPosition == 1 ) this.rotateToPosition = -1;
	else this.RequireDraw();
}
		
//If the user is currently dragging, then track the position of the 
//mouse and keep the starting point of the drag locked to that position
//by turning the globe by the angle that best represents this
//source: http://rainwarrior.thenoos.net/dragon/arcball.html
Gl.Globe.tQuat1 = new Gl.Quat();
Gl.Globe.tQuat2 = new Gl.Quat();
Gl.Globe.tVec1 = new Gl.Vector3();
Gl.Globe.tVec2 = new Gl.Vector3();
Gl.Globe.tVec3 = new Gl.Vector3();
Gl.Globe.prototype.DoDrag = function(dt)
{		
	//Get the current mouse point in world coords
	var dragEndPoint = Gl.Globe.tVec1;
	this.PixelToPoint(this.canvasMouseX, this.canvasMouseY, dragEndPoint);
				
	this.mGlobe.TransformPoint(dragEndPoint);
		
	//if the current mouse point is not on the globe, then give up.
	if( isNaN(dragEndPoint.x) ) 
	{
		this.dragStartPoint.x = NaN;
		return;
	}
			
	//Convert the drag start and current points to directions
	var dragStartPoint = Gl.Globe.tVec2;
	dragStartPoint.SetVector3(this.dragStartPoint);
			
	dragStartPoint.Normalize();
	dragEndPoint.Normalize();
			
	//The cross product of those directions gives the axis to rotate around
	//and the dot product of those directions gives the cosine of the angle
	//to rotate through (so use acos to turn back into an actual angle)
	var axis = Gl.Globe.tVec3;
	axis.SetCross(dragStartPoint, dragEndPoint);
	if( axis.LengthSquared() < 0.001 ) return;
			
	var angle = Math.acos(dragStartPoint.Dot(dragEndPoint));
		
	//Modify the current angle with the quaternion that describes the axis
	//and angle.  Save the current rotation before we do, as we will need it 
	//shortly.
	Gl.Globe.tQuat1.SetAsQuat(this.quat);
			
	Gl.Globe.tQuat2.SetFromAxisAndAngle(axis.x, axis.y, axis.z, angle);
	Gl.Globe.tQuat2.Normalize();
			
	Gl.Globe.tQuat2.Mul(this.dragStartAng);
	this.quat.SetAsQuat(Gl.Globe.tQuat2);
			
	//Get the difference between the angle now and the angle we just had.
	//we changed that angle in a single frame, so that change is our new 
	//angular velocity.
	Gl.Globe.tQuat2.SetAsQuat(this.quat);
	Gl.Globe.tQuat2.MulConjugate(Gl.Globe.tQuat1);
	this.angVel.x = Gl.Globe.tQuat2.x;
	this.angVel.y = Gl.Globe.tQuat2.y;
	this.angVel.z = Gl.Globe.tQuat2.z;	
	
	this.angVel.Div(dt);
	
	this.RequireDraw();
}
		
//Returns true if the globe is currently rotating to a fixed point via RotateTo()
Gl.Globe.prototype.IsCentering = function()
{
	return (this.rotateToPosition >= 0);
}
		
//Returns true is the user is currently dragging the globe.		
Gl.Globe.prototype.IsDragging = function()
{
	return !isNaN(this.dragStartPoint.x);
}
		
//Each frame: Get the new rotation, and render the geometry
Gl.Globe.prototype.Draw = function()
{	
	this.clock.NextFrame();
	
	$(this).trigger( $.Event("frame") );		   	
	
	if( this.requireDraw == false && this.newImage == false ) return;
	this.requireDraw = false;
		
	
	var dt = this.clock.Delta();
	
	//Only integrate (update the angular velocity) if the user is not interacting with it.
		 if( this.IsCentering()) this.DoRotate(dt);
	else if( this.IsDragging() ) this.DoDrag(dt);
	else
	{
		//Apply the damping to the velocity.  If velocity is low, stop re-drawing
		//this.angVel.Mul( (this.damping * this.damping) * dt);
		this.angVel.Mul( Math.pow(this.damping, dt));
		this.quat.AddScaledVector(this.angVel, 1 * dt);
		
		if( this.angVel.LengthSquared() > 0.0001 ) this.RequireDraw();
	}
			
	this.quat.Normalize();
	this.RecalcGlobeMatrix();
				
	var len;
	var i;
	var v;
	var f;
			
	//Combine the matrix representing the globe, and the matrix representing
	//the camera into a single matrix, with which we will project the globe
	//points onto 2 dimensions.
	this.mFrame.SetAsMatrix44(this.mCam);
	this.mFrame.Mul(this.mGlobe);
			
	//Transform all the verticies
	len = this.arrVerticies.length;
	for(i = 0 ; i < len ; i++)
	{
		v = this.arrVerticies[i];
		v.pd.SetVector3(v.pu);
		
		this.mFrame.TransformPointPerspective(v.pd);
		//Perspective divide.
		v.pd.x /= v.pd.z ;
		v.pd.y /= v.pd.z ;
	}
			
	//For each face, work out its signed area in terms of the screen.  This
	//is similar to the system OpenGL uses to ascertain if a triangle is back
	//facing.
	len = this.arrFaces.length;
	for(i = 0 ; i < len ; i++)
	{
		f = this.arrFaces[i];
				
		if( this.TriSignedArea2D(f.va.pd.x, f.va.pd.y, f.vb.pd.x, f.vb.pd.y, f.vc.pd.x, f.vc.pd.y) < 0 && this.cullBackFaces) continue;
		f._z = 0.3333333 * ( f.va.pd.z + f.vb.pd.z + f.vc.pd.z );
		this.arrRenderList.push(f);
	}
			
	//Sort the list of triangles to be drawn from back to front (painters algorithm)
	this.arrRenderList.sort(Gl.Globe.FaceSort);
	
	this.context.save();
	
	this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	
	//Finally, simply loop through and draw the faces.
	if( this.img != null && this.img.width != 0 )
	{
		
	
		try
		{
			len = this.arrRenderList.length;
			for(i = 0 ; i < len ; i++)
				this.arrRenderList[i].Draw(this.context, this.img);
						
			if( this.img.complete ) this.newImage = false;
		}
		
		catch(err) {}
	}
				 
	this.arrRenderList.length = 0;
	
	this.context.restore();		
	
	//might be a useful thing to know
	$(this).trigger( $.Event("redraw") );		   	
}

//Helper function to sort the faces by Z-order
Gl.Globe.FaceSort = function(a, b)
{
		 if( a._z > b._z ) return  -1;
	else if( a._z < b._z ) return   1;
	else return 0; 
}

//Helper function to perform a linear interpolation (lerp)
Gl.Globe.prototype.Lerp = function(a, b, x)
{
	return a + ((b - a) * x);
}
		
//Helper function to work out the signed area of a triangle in 2D
Gl.Globe.prototype.TriSignedArea2D = function(x1, y1, x2, y2, x3, y3)
{
	return 0.5 * (-x2 * y1 + x3 * y1 + x1 * y2 - x3 * y2 - x1 * y3 + x2 * y3);
}
		
//Keep the globe matrix in sync with the quat representing its rotation.
//Globe is centered on world zero, so no translation required.
Gl.Globe.prototype.RecalcGlobeMatrix = function()
{
	this.quat.GetMatrix44(this.mGlobe);
}
		
//Create a matrix representing the camera.  This is the combined matrix
//of the viewport, projection, and camera position.
Gl.Globe.prototype.RecalcCameraTransform = function()
{
	var mPort = new Gl.Matrix44();
	var mProj = new Gl.Matrix44();
	var mView = new Gl.Matrix44();
	
	mPort.SetAsViewport(0, this.canvas.width, 0, this.canvas.height);		
	mProj.SetAsPerspective(Gl.Globe.CAM_FOV, 1.0, Gl.Globe.CAM_NEAR_PLANE, Gl.Globe.CAM_FAR_PLANE);
	mView.SetAsTranslation(0, 0, -Gl.Globe.CAM_Z);
	
	this.mCam.SetAsMatrix44(mPort);
					
	this.mCam.Mul(mProj);
	this.mCam.Mul(mView);
}
	

