
var Gl = Gl || {}

//Utility maths class to hold a 2-component vector
Gl.Vector2 = function() {}

Gl.Vector2.prototype.x = 0.0;
Gl.Vector2.prototype.y = 0.0;

//Sets this vector to hold the supplied point
Gl.Vector2.prototype.SetXY = function(x, y)
{
	this.x = x;
	this.y = y;
}

//Sets this vector to equal the same as the supplied vector
Gl.Vector2.prototype.SetVector2 = function(v)
{
	this.x = v.x;
	this.y = v.y;
}

//Returns the squared length (magnitude) of this vector     
Gl.Vector2.prototype.LengthSquared = function()
{
   	return (this.x * this.x + this.y * this.y );
}

		
//Returns the dot (inner) product of this vector with the supplied vector
Gl.Vector2.prototype.Dot = function(v)
{
	return (this.x*v.x + this.y*v.y );
}
		
//Normalizes this vector to create a unit vector.
Gl.Vector2.prototype.Normalize = function()
{
	var n = (1.0/Math.sqrt(this.x*this.x + this.y*this.y) );
	this.x *= n;
	this.y *= n;
}
    	
//Adds the supplied vector to this vector
Gl.Vector2.prototype.Add = function(v)
{
	this.x += v.x;
	this.y += v.y;
}
		
//Subtracts the supplied vector from this vector
Gl.Vector2.prototype.Sub = function(v)
{
	this.x -= v.x;
	this.y -= v.y;
}
		
//Multiplys (scales) this vector by the supplied number
Gl.Vector2.prototype.Mul = function(s)
{
	this.x *= s;
	this.y *= s;
}
		
//Divides (reduces) this vector by the supplied number
Gl.Vector2.prototype.Div = function(s)
{
	this.x /= s;
	this.y /= s;
}
		
//Sets all components of this vector to zero.
Gl.Vector2.prototype.SetZero = function()
{
	this.x = 0;
	this.y = 0;
}




