
//Helper class to simplify the use of 'markers' - or DOM elements that maintin their position on the globe.
var Gl = Gl || {}

//Takes a refernece to an existing globe, and optional offset for all markers
Gl.Markers = function(globe, handlex, handley)
{
	this.arrMarkers = [];
	this.globe = globe;
	this.handlex = handlex || 0;
	this.handley = handley || 0;
	$(globe).on("redraw", this, Gl.Markers.OnGlobeRedraw);
};

Gl.Markers.prototype.arrMarkers = null;
Gl.Markers.prototype.globe 	    = null;
Gl.Markers.prototype.handlex 	= 0;
Gl.Markers.prototype.handley	= 0;

//Adds a single marker, in the form of a DOM element
Gl.Markers.prototype.AddMarkers = function(markers)
{
	for(var i = 0 ; i < markers.length ; i++)
		this.AddMarker(markers[i]);
};

//Adds an array of DOM elements
Gl.Markers.prototype.AddMarker = function(element)
{
	this.arrMarkers.push(element);
	
	var clickable = element.getAttribute("data-clickable") || "true";
	if( clickable === "true" )
	{
		$(element).on("mouseup", { self: this, marker: element}, Gl.Markers.OnMouseUpMarker);
	}
};

//Event handler for globe redraw - rotation is presumed to have changed
Gl.Markers.OnGlobeRedraw = function(e)
{
	e.data.OnGlobeRedraw();	
};

//Event handler for a marker 'click' - but mouse up is more compatible with browsers
Gl.Markers.OnMouseUpMarker = function(e)
{
	e.data.self.OnMouseUpMarker(e.data.marker);
};

//Rotate the globe to show the marker position
Gl.Markers.prototype.OnMouseUpMarker = function(marker)
{
	var lat = parseFloat(marker.getAttribute('data-lat')) || 0.0;
	var lng = parseFloat(marker.getAttribute('data-lng')) || 0.0;
	var menuItem = marker.getAttribute('data-id') || false;
	if(menuItem) {
		var menu = $(document.getElementById(menuItem))
		$('.city').not(menu).hide();
		menu.fadeToggle();
	}
	
	this.globe.RotateTo(lat, lng);
};

//If the globe rotation has changed, the screen coord of the markers must be updated
Gl.Markers.prototype.OnGlobeRedraw = function()
{
	var p = new Gl.Vector2();
	var c = $(this.globe.GetCanvas()).offset();
	
	//Use the "data-" functionality of HTML5 to store lat/lng info
	for(var i = 0 ; i < this.arrMarkers.length ; i++)
	{
		var marker = this.arrMarkers[i];
		var lat 	= parseFloat(marker.getAttribute('data-lat')) 	  || 0.0;
		var lng 	= parseFloat(marker.getAttribute('data-lng'))     || 0.0;
		var radius  = parseFloat(marker.getAttribute('data-radius'))  || Gl.Globe.GEOM_RADIUS;
		
		var z = this.globe.LatLngToPixel(lat, lng, radius, p);
		
		$(marker).offset( { left: (p.x + c.left) - this.handlex, top: (p.y + c.top) - this.handley } );
		
		if( z < 3.0 ) $(marker).hide();
		else 		  $(marker).show();
		
	}
};


