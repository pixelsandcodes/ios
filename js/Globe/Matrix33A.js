
var Gl = Gl || {}

//Class to hold a flash-style 3x3 Affine matrix.  Last row is implied.
Gl.Matrix33A = function() {}

Gl.Matrix33A.prototype.a  = 0.0;
Gl.Matrix33A.prototype.b  = 0.0;
Gl.Matrix33A.prototype.c  = 0.0;
Gl.Matrix33A.prototype.d  = 0.0;
Gl.Matrix33A.prototype.tx = 0.0;
Gl.Matrix33A.prototype.ty = 0.0;
   

//Set this matrix to be the result of multiplying this by the supplied matrix
Gl.Matrix33A.prototype.Concat = function(matrix)
{
	var a  = matrix.a * this.a + matrix.c * this.b;
    var b  = matrix.b * this.a + matrix.d * this.b;
    var c  = matrix.a * this.c + matrix.c * this.d;
    var d  = matrix.b * this.c + matrix.d * this.d;
    var tx = matrix.a * this.tx + matrix.c * this.ty + matrix.tx;
    var ty = matrix.b * this.tx + matrix.d * this.ty + matrix.ty;
    
    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
    this.tx = tx;
    this.ty = ty;
}

//Set this matrix to its own inverse.
Gl.Matrix33A.prototype.Invert = function()
{
    var det = this.a * this.d - this.b * this.c;
  
    var a =  this.d / det;
    var b = -this.b / det;
    var c = -this.c / det;
    var d =  this.a / det;
    var tx= (this.c * this.ty - this.d * this.tx) / det;
    var ty= (this.b * this.tx - this.a * this.ty) / det;
    
    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
    this.tx = tx;
    this.ty = ty;
}
  
  
  