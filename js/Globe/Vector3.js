
var Gl = Gl || {}

//Utility maths class to hold a 3-component vector
Gl.Vector3 = function() {}

Gl.Vector3.prototype.x = 0.0;
Gl.Vector3.prototype.y = 0.0;
Gl.Vector3.prototype.z = 0.0;

//Returns the squared length (magnitude) of this vector     
Gl.Vector3.prototype.LengthSquared = function()
{
   	return (this.x * this.x + this.y * this.y + this.z * this.z);
}

//Sets this vector to hold the supplied point
Gl.Vector3.prototype.SetXYZ = function(x, y, z)
{
	this.x = x;
	this.y = y;
	this.z = z;
}

//Sets this vector to equal the same as the supplied vector
Gl.Vector3.prototype.SetVector3 = function(v)
{
	this.x = v.x;
	this.y = v.y;
	this.z = v.z;
}

//Sets this vector to be the cross (outer) product of the two supplied vectors
Gl.Vector3.prototype.SetCross = function(v1, v2)
{
   	var x;
	var y;
	var z;

   	x = v1.y*v2.z - v1.z*v2.y;
   	y = v2.x*v1.z - v2.z*v1.x;
   	z = v1.x*v2.y - v1.y*v2.x;
   	this.x = x;
   	this.y = y;
   	this.z = z;
}
		
//Returns the dot (inner) product of this vector with the supplied vector
Gl.Vector3.prototype.Dot = function(v)
{
	return (this.x*v.x + this.y*v.y + this.z*v.z);
}
		
//Normalizes this vector to create a unit vector.
Gl.Vector3.prototype.Normalize = function()
{
	var n = (1.0/Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z));
	this.x *= n;
	this.y *= n;
	this.z *= n;
}
    	
//Adds the supplied vector to this vector
Gl.Vector3.prototype.Add = function(v)
{
	this.x += v.x;
	this.y += v.y;
	this.z += v.z;
}
		
//Subtracts the supplied vector from this vector
Gl.Vector3.prototype.Sub = function(v)
{
	this.x -= v.x;
	this.y -= v.y;
	this.z -= v.z;
}
		
//Multiplys (scales) this vector by the supplied number
Gl.Vector3.prototype.Mul = function(s)
{
	this.x *= s;
	this.y *= s;
	this.z *= s;
}
		
//Divides (reduces) this vector by the supplied number
Gl.Vector3.prototype.Div = function(s)
{
	this.x /= s;
	this.y /= s;
	this.z /= s;
}
		
//Sets all components of this vector to zero.
Gl.Vector3.prototype.SetZero = function()
{
	this.x = 0;
	this.y = 0;
	this.z = 0;
}




