window.Boxes = function(element, video) {
    
    // return immediately if element doesn't exist
    if (!element) throw new Error("element is not defined");

    var _this = this;
    
    this.wrapper = $(element);
    this.originalBoxes = $(this.wrapper).children();
    this.boxes = this.originalBoxes;
    this.viewAll = $("#view-all");
    this.loading = $('<img />').attr({'src' : 'png/loader.gif', 'class':'loader'});
    this.fullscreenWrapper = $("#fullscreen-wrapper");
    this.video = video || '';
    this.count = 3;
    this.MapPosition = null;
    this.BoxPosition = null;
    this.fullscreen = false;
    this.animating = false;
    this.activeCategory = 'View-All';
    
    // Evenet Handlers
    $(".box_item").bind("click", function(e) {
        if ($(this).hasClass("view-all"))
            return _this.categoryClicked(e, $("a#view-all-category"));
        if (!$(this).hasClass("detailed"))
            _this.boxClicked($(this));
    });

    $(".box_item .closesign").live("click", function(e) {
        _this.closeDetailedBox(e, $(this));
    });
    
    $("#fullscreen-wrapper .closesign").live("click", function(e) {
         _this.revertFullScreenBox($(this).parent().parent().parent());
    });

    $("#fullscreen-video-wrapper .text-closesign-fullscreen").live("click", function(e) {
         _this.hideVideo($(this).parent());
    });

    $(".box_item .read").live("click", function(e) {
        e.preventDefault();
        _this.showReadMore();
    });

    $(".read-more").live("click", function(e) {
        e.preventDefault();
        _this.showFullScreenText();
    });

    $(".read-more.fullscreen").live("click", function(e) {
        e.preventDefault();
        _this.showFullScreenText(true);
    });
    
    $(".zoom").live("click", function(e) {
        e.preventDefault();
        _this.showFullScreen(false);
    });
    
    $(".share").live("hover", function(e) {
        e.preventDefault();
        $(this).find('span').fadeToggle(200);
        $(this).siblings().fadeToggle(200);
        _this.detailedBox.find('.links').fadeToggle(200);
    });

    $(".text-closesign").live("click", function(e) {
        e.preventDefault();
        _this.resizeTextOverlays();
        $(this).parent().fadeOut(200);
    });

    $(".text-closesign-fullscreen").live("click", function(e) {
        e.preventDefault();
        _this.detailedBox.myScroll.remove();
        _this.resizeTextOverlays();
        $(this).parent().fadeOut(200);
    });

    $("#categories a").bind("click", function(e) {
        _this.categoryClicked(e, $(this));
    });
    
    $(document).keyup(function(e) {
      if (e.keyCode == 27) {
          _this.revertFullScreenBox();
      }
    });

    $( "#search-filter" ).autocomplete({
      dataType: 'json',
      source: function( request, response ) {
        $.ajax({
          url: "/search",
          data: { term: request.term},
          type: "POST",
          dataType: "json",
          success: function( data ) {
            response( $.map( data, function( item ) {
                    return item;
                })
            );
          }
        });
      },
      minLength: 2,
      select: function(event, ui) {
        this.value = ui.item.label;
        var element = document.getElementById(ui.item.box);
        _this.categoryClicked(event, $('#view-all-category'));
        setTimeout(function() {
            _this.boxClicked($(element));
            $('html,body').animate({
                scrollTop: (parseInt(element.style.top)) - _this.spacing() + 'px'
            }, 600); 
        }, 200);
        return false;
      }
    });
    
    window.addEventListener('resize', this, false);
    
    this.resizeBoxes(this.boxes, this.count);
    this.history();
    
};

Boxes.prototype = {
    
    history: function() {
        
        var obj = this;
        $.history.init(setupBox);
        
        function setupBox(hash) {
            if(hash) {
                //obj.createNewHashTag();
                var hashMap = obj.readHashTag(hash);

                var id = hashMap.get('box');
                var cat = hashMap.get('cat');

                if (id !== '') {
                    var element = document.getElementById(hashMap.get('box'));
                    
                    if(element.nodeType == Node.ELEMENT_NODE) {
                        if(obj.detailedBox == undefined)
                            loadBox(element);
                        else if(id != obj.detailedBox.attr('id')){
                            loadBox(element);
                        }
                        else
                            return false;
                    }
                } else obj.closeDetailedBox();
                
                if (cat !== '') {
                    var event = new Event("click");
                    var element;
                    $("#categories ul li").each(function(){
                        if($(this).attr('rel') === cat)
                            element = $(this);
                    })                    
                    if(cat !== obj.activeCategory)
                        obj.categoryClicked(event, $(element));
                }
            } else obj.closeDetailedBox();
        }
        
        function loadBox(element) {
            setTimeout(function() {
                $('html,body').animate({
                    scrollTop: (parseInt(element.style.top)) - obj.spacing() + 'px'
                }, 600);
                obj.closeDetailedBox();
                obj.boxClicked($(element));
            }, 200);
        }
    },
    
    resizeBoxes: function(boxes, count) {

        var perRow = (typeof count == 'undefined' || isEmpty(count)) ? this.count : count;
        if(typeof boxes === 'undefined' || boxes ===  null)
            throw new Error("Boxes are undefined !");
        
        var multi =[];

        var index = 0;
        while(index != boxes.length) {
            var offset = index + perRow <= boxes.length ? index + perRow : boxes.length;

            if (index > boxes.length) {
                break;
            }
            var a = boxes.slice(index, offset);
            var map = new HashMap();
            var add = false;
            for (var i=0; i < a.length; i++) {

                if(this.detailedBox != undefined) {
                   if($(a[i]).attr("id") == this.detailedBox.attr("id")) {
                        add = true;
                        this.MapPosition = index / perRow;
                        this.BoxPosition = i;
                   }
                }else {
                        this.MapPosition = null;
                        this.BoxPosition = null;
                   }
                map.put(i,a[i]);
                
            }

            multi.push(map);
            if(add)
                multi.push(new HashMap());
            
            index+=perRow;
        }
        for (var i=0; i < multi.length; i++) { 

            var windowWidth = this.windowWidth();
            var boxWidth =this.smallSize();
            var spacing = this.spacing();
            
            // If the map contains Detailed Box
            if(this.MapPosition != null && this.MapPosition == i ) {
                var count = 0;
                for(var x=0; x < multi[i].size() ; x++) {
                    var obj = this;
                    var el = $(multi[i].get(x));

                     // Detailed Box Resize
                     if(el.attr("id") == this.detailedBox.attr("id")) {

                        var left = x == 0 ? spacing : this.smallSize() + 2 * spacing;
                        var top = i * this.smallSize()+ (i+1) * spacing;
                        var bigSize = this.bigSize();
                        this.detailedBox.css({
                            "top" : top + "px",
                            "left" : left + "px",
                            "width" : bigSize + "px",
                            "height" : bigSize + "px"
                        });

                     } else {

                         el.width(boxWidth).height(boxWidth);

                         if(this.BoxPosition == 0) {
                             var top = (i + count) * el.outerHeight()+ (i + 1 + count) * spacing;
                             var left = 2 * ( el.outerWidth() + spacing ) + spacing;
                         } else {
                             var top = (i + count) * el.outerHeight()+ (i + 1 + count) * spacing;
                             var left = spacing;
                         }                             

                         el.css({"top" : top, "left" : left});

                         count++;
                     }

                }// End FOR

             }// End IF

             // Default Resizing and Positioning
             else {
                 for(var x=0; x < multi[i].size() ; x++) {

                     var el = $(multi[i].get(x));

                     el.width(boxWidth).height(boxWidth);

                     var top = i * el.outerHeight()+ (i+1) * spacing;
                     var left = x * el.outerWidth() + (x+1) * spacing;

                     el.css({"top" : top, "left" : left});


                 }// End FOR

             }// End ELSE

         }// End FOR

         // Set Height Of The Box Wraper
         this.wrapper.css('height', multi.length * (this.smallSize() + this.spacing()) + this.spacing());

    },// End FUNCTION
    
    // Category Clicked
    categoryClicked : function(e, element) {

        e.preventDefault();
        var obj = this;
            
        $("#categories a").removeClass("selected");
        element.addClass("selected");

        var boxes = [];
        var filter = element.attr("rel");
        
        // Filter selected boxes
        if(filter === 'View-All') {
            obj.activeCategory = 'View-All';
            obj.originalBoxes.each(function(){
                $(this).removeClass("hidden").fadeIn(200);
                obj.viewAll.css('display','none');
                if(!$(this).hasClass("view-all"))
                    boxes.push($(this));
            });
        } else {
            obj.activeCategory = element.attr("rel");
            obj.originalBoxes.each(function(){
                if($(this).hasClass(filter)) {
                    $(this).removeClass("hidden").stop().fadeIn(200);
                    boxes.push($(this));
                } else {
                    if(!$(this).hasClass("view-all")) {
                        $(this).addClass("hidden").stop().fadeOut(200);
                        $(this).css({top : 0, left : 0});
                    }
                }
            });

            obj.viewAll.css("display", "block");

            boxes.push(obj.viewAll);
        }
        // Set New Boxes and Resize Boxes
        obj.boxes = boxes;
        
        if(this.fullscreen)
             this.revertFullScreenBox();
        obj.closeDetailedBox(e);
        obj.createNewHashTag();
        obj.resizeBoxes(obj.boxes, obj.count);

    },
    
    boxClicked: function(element) {
        var obj = this;
        var boxes =[];
        var parent = element;
        var filter = "hidden";
        var selectedID = parent.attr("id");
        var originalWidth = parent.outerWidth();

        if(!parent.hasClass("detail")) {
            
            var width = obj.bigSize();

            for (var i=0;i<obj.originalBoxes.length;i++)
            {
                var b = $(obj.originalBoxes[i]);
                
                if(b.hasClass('detail'))
                    obj.closeDetailedBox();
                
                if(!b.hasClass(filter) && b.attr("id") != selectedID)
                    b.filter(".detail").stop().animate({width: originalWidth}, 1000).removeClass("detail");

                if(!b.hasClass(filter))
                    boxes.push(b);

            }
            this.detailedBox = parent;
            
            obj.loadDetails(element.attr("data-url"), parent,function(data) {
                initSwipe();

                /** Setup Video Slides **/
                var videoSlides = data.find('.video-slide');
                if(videoSlides.length > 0) {
                    obj.detailedBox.videoDetail = new HashMap();
                    videoSlides.each(function() {
                        var id = $(this).find('video').attr('id');
                        obj.detailedBox.videoDetail.putVideo(id);
                        obj.startVideo(id, obj.detailedBox.videoDetail.get(id), $(this).attr("data-slide"));
                    });
                } else if(obj.detailedBox.videoDetail instanceof HashMap)
                    obj.detailedBox.videoDetail.forEach(function(el) {
                        console.log(el);
                        el.destroy();
                    });
                
                if(parent.attr("data-loaded") !== "true")
                    setTimeout(function() {
                         parent.attr("data-loaded", "true");
                        return obj.showReadMore();
                    }, 600);
                
            });
            
            parent.addClass("detail")
                .css({left: parent.outerWidth() * 1.30, top:  parent.outerWidth() * 0.11})
                .stop().animate({width: width, height: width}, 600, function() {
                    obj.createNewHashTag();
                });
                
            // Set New Boxes and Resize Boxes
            obj.boxes = boxes;
            obj.resizeBoxes(obj.boxes);
        }

    },
    
    loadDetails: function(url, element, callback, showLoading){
        
        var obj = this;
        var original = element.find(".original");
        var ajax = element.find(".ajax");
        showLoading = showLoading === undefined ? true : showLoading;
        original.css('display', 'none');
        if(showLoading)
            element.append(this.loading.fadeIn(100));
        
        var startTime = new Date().getTime();
        $.ajax({
          url: url,
          success: function(data){
              var img = $(data).find('img').length;
              var loaded = 0;

              if(img === 0) {
                  showData();
              } else {
                  $(data).find('img').bind('load', function() {
                      loaded++;
                      if(loaded === img) {
                          var timeDiff = (new Date().getTime() - startTime);
                          setTimeout(function() {
                              showData();
                          }, 600 - timeDiff, true);
                      }

                  }).bind('error', function() {
                    if(showLoading)
                        element.find(obj.loading).fadeOut(100).remove();
                    else
                        $("#loaderImage").fadeOut(600);
                    ajax.html('<p class="error">Images can not be loaded.</p>');
                    ajax.fadeIn(600);
                  });
              }
              
              function showData() {

                if(showLoading)
                  element.find(obj.loading).fadeOut(600, function() {
                    $(this).remove();
                  });
                ajax.html($(data));
                ajax.delay(200).fadeIn(600, function() {
                    if (callback !== undefined)
                        callback($(data));
                });
                
              }
              
            
          },
          error: function(e, x, settings, exception) {
                element.find(obj.loading).fadeOut(100).remove();
                $("#loaderImage").fadeOut(300);
                var message;
                var statusErrorMap = {
                    '400' : "Server understood the request but request content was invalid.",
                    '401' : "Unauthorised access.",
                    '403' : "Forbidden resouce can't be accessed",
                    '404' : "The system is unable to find the requested action.",
                    '500' : "Internal Server Error.",
                    '503' : "Service Unavailable"
                };
                if (e.status) {
                    message =statusErrorMap[e.status];
                                    if(!message){
                                          message="Unknow Error \n.";
                                      }
                }else if(x=='parsererror'){
                    message="Error.\nParsing JSON Request failed.";
                }else if(x=='timeout'){
                    message="Request Time out.";
                }else if(x=='abort'){
                    message="Request was aborted by the server";
                }else {
                    message="Unknow Error \n.";
                }
              
                element.find(obj.loading).fadeOut(100).remove();
                ajax.html('<p class="error">'+message+'</p>');
                ajax.fadeIn(200);
          }
       });
    },
    
    showReadMore: function(element) {
            this.resizeTextOverlays();
            element = element !== undefined ? element : this.detailedBox;
            var overlay = element.find(".text-overlay");
            overlay.fadeToggle(400);
    },
    
    showFullScreenText: function() {
            var obj = this;
            var element = $("#fullscreen-wrapper").find(".text-overlay");
            if(this.fullscreen)
                $("#fullscreen-wrapper").find(".text-overlay").fadeOut(400);
            else
                this.detailedBox.find(".text-overlay").fadeToggle(400);

            this.resizeTextOverlays();
            obj.detailedBox.myScroll = obj.detailedBox.find('#iScroll').niceScroll({
                    cursorcolor : "#AAA",
                    cursoropacitymin : 0.1,
                    cursoropacitymax : 0.6,
                    cursorborder : 0,
                    cursorwidth : '4px',
                    cursorborderradius : '2px',
                    railpadding : {top: '0'}
            });
            setTimeout(function() {
                obj.detailedBox.myScroll.resize();
            }, 600);

            this.detailedBox.find(".text-overlay-fullscreen").fadeIn(400);
                
                
    },
    
    showFullScreen: function(show, showLoading) {
        
        show = show !== undefined ? show : true;
        showLoading = showLoading !== undefined ? showLoading : true;
        
        var obj = this;
        if(showLoading)
            $("#loaderImage").fadeIn(200);

        if(!this.fullscreen) {
            var c = document.getElementById("content");

            this.detailedBox.find(".text-overlay").fadeOut(300);
            this.pauseVideo(this.detailedBox.videoDetail);
            this.fullscreen = true;
            $('body').css('overflow-y', 'hidden');
            obj.fullscreenWrapper.css({
                "width"   : obj.windowWidth(),
                "height"  : obj.windowHeight(),
                "display" : 'block'
            });
            
            obj.loadDetails(obj.detailedBox.attr("detailed-url"), obj.fullscreenWrapper, function(data) {
                obj.resizeTextOverlays();

                /** Setup Video Slides **/
                var videoSlides = data.find('.video-slide');
                if(videoSlides.length > 0) {
                    obj.detailedBox.videoDetailFullscreen = new HashMap();
                    videoSlides.each(function() {
                        var id = $(this).find('video').attr('id');
                        obj.detailedBox.videoDetailFullscreen.putVideo(id);
                        obj.startVideo(id, obj.detailedBox.videoDetailFullscreen.get(id), $(this).attr("data-slide"), true);
                    });
                } else if(obj.detailedBox.videoDetailFullscreen instanceof HashMap)
                    obj.detailedBox.videoDetailFullscreen.forEach(function(el) {
                        console.log(el);
                        el.destroy();
                    });

                initFullscreenSwipe();
                 
                $("#loaderImage").fadeOut(200, function() {
                    if(show)
                        obj.showReadMore(obj.fullscreenWrapper);
                });
            }, false);
            
        }

    },

    startVideo: function(id, video, url, fullscreen){
        var obj = this;
        var videoElement = document.getElementById(id);

        $.getJSON(url, function(data) {
            video.src(data);
            if(fullscreen === undefined) {
                resizeVideoDetail();
                window.addEventListener('resize', resizeVideoDetail(), false);
            }
            else {
                resizeVideoFullscreen();
                window.addEventListener('resize', resizeVideoFullscreen, false);
            }
                
        });

        function resizeVideoDetail() {
            video.height($('#swipe-wrapper').width()).width($('#swipe-wrapper').width());
        }

        function resizeVideoFullscreen(){
            video.height(obj.windowHeight()*0.92).width(obj.windowWidth());
           
        }

    },

    pauseVideo: function(map) {
        if(map instanceof HashMap)
            if(map.size() > 0)
                for (var key in map) {
                    if (map.hasOwnProperty(key)) {
                        if(!map[key].paused())
                            map[key].pause();
                    }
                }
        
    },

    destroyVideo: function(map){
        for (var key in map) {
            if (map.hasOwnProperty(key)) {
                map[key].destroy();
            }
        }
    },

    loadNextFullscreen: function () {
        var index;
        var obj = this;
        for (var i = this.boxes.length - 1; i >= 0; i--) {
            if(this.detailedBox.attr('id') === this.boxes[i].attr('id')) {
                if((i+1) === this.boxes.length || this.boxes[i+1].attr('id') === 'view-all')
                    index = 0;
                else
                    index = i+1;
            }
        }
        this.loadFullscreenBox(index);
    },

    loadPrevFullscreen: function () {
        var index;
        var obj = this;
        for (var i = this.boxes.length - 1; i >= 0; i--) {
            if(this.detailedBox.attr('id') === this.boxes[i].attr('id')) {
                if(i === 0)
                    index = this.boxes.length - (this.boxes[this.boxes.length - 1].attr('id') === 'view-all' ? 2 : 1);
                else
                    index = i-1;
            }
        }
        this.loadFullscreenBox(index);
    },

    loadFullscreenBox: function(index) {
        var obj = this;
        this.fullscreen = false;
        $("#loaderImage").fadeIn(200);
        this.fullscreenWrapper.find("#swipe-wrapper-fullscreen").fadeOut(600, function() {            
            obj.fullscreenWrapper.find(".ajax").html("");
        });

        obj.destroyVideo(obj.detailedBox.videoDetailFullscreen);
        obj.closeDetailedBox();
        obj.boxClicked(obj.boxes[index]);

        setTimeout(function() {
                 obj.showFullScreen(true, false);
                 //obj.showReadMore(obj.fullscreenWrapper);
            }, 1200);
    },
    
    closeDetailedBox: function() {
        // Remove Detailed Box and Resize Boxes
        if(this.detailedBox !== undefined) {
            if(this.detailedBox.myScroll) {
                try
                  {
                    this.detailedBox.myScroll.remove();
                  }
                catch(err)
                  {
                  //Handle errors here
                  }
            }
            this.revertBox(this.detailedBox);
            this.destroyVideo(this.detailedBox.videoDetail);
            this.detailedBox = undefined;
            this.MapPosition = null;
            this.BoxPosition = null;
            this.resizeBoxes(this.boxes);
            $("#twitter-wjs").remove();
            unbindSwipe();
        }
    },
    
    revertBox: function(el) {
        var obj = this;
        var element = el === undefined ? this.detailedBox : el;
        
        element.stop().animate({
            width : this.smallSize(),
            height: this.smallSize()
        }, 600, function() {
            obj.createNewHashTag();
        });

        element.find(".original").fadeIn(600);
        element.find(".ajax").fadeOut(100, function() {
            $(this).html("");
        });

        element.removeClass("detail");
        
    },
    
    revertFullScreenBox: function() {

        var obj = this;
        var element = obj.fullscreenWrapper;

        this.fullscreen = false;
        this.resizeTextOverlays();
        if(obj.detailedBox.myScroll)
            setTimeout(obj.detailedBox.myScroll.resize, 600);
        
        var width = this.bigSize();
        $('body').css('overflow-y', 'visible');
        element.css('display', 'none').find(".ajax").fadeOut(10, function() {
            obj.destroyVideo(obj.detailedBox.videoDetailFullscreen);
            $(this).html("");
        });
        
        obj.resizeBoxes(obj.boxes);

    },
    
    handleEvent: function(e) {
    switch (e.type) {
        case 'resize':this.resize();break;
        }
    },
    
    resize: function() {
        var obj = this;
        this.resizeBoxes(this.boxes, this.count);
        this.resizeTextOverlays();
        this.resizeFullscreen();
        if(obj.detailedBox.myScroll)
            setTimeout(obj.detailedBox.myScroll.resize, 600);
    },
    
    resizeFullscreen: function() {
        var obj = this;
        if(this.fullscreen)
            this.fullscreenWrapper.css({
                "width"   : obj.windowWidth(),
                "height"  : obj.windowHeight()
            });
    },
    
    resizeTextOverlays: function() {
        if(this.detailedBox !== undefined) {
            var text = $(".text-overlay");
            var fullscreen = $(".text-overlay-fullscreen");
            var fullscreenText = $("#fullscreen-wrapper .text-overlay, #fullscreen-video-wrapper");
            
            var width = Math.round(this.wrapperWidth() * 0.475585937);
            var height = Math.round(width * 0.513471901);

            if(this.fullscreen)
                fullscreen.css({
                    width    : width,
                    height   : width - 10,
                    top       : ( window.innerHeight - width - 10 ) / 2, 
                    left      : ( window.innerWidth - width ) / 2, 
                    position  : 'fixed',
                    paddingTop: '10px',
                    'z-index' : 1000});
            else
                fullscreen.css({
                    width    : this.bigSize(),
                    height   : this.bigSize(),
                    top      : 0,
                    left     : 0,
                    paddingTop: '10px',
                    position : 'absolute',
                    'z-index': 130
                });

            fullscreen.find('.text-wrapper').height(fullscreen.height() * 0.86963434022);

            text.css({
                width  : width,
                height : height,
                top    : (this.bigSize() - height) / 2,
                left   : (this.bigSize() - width) / 2
            });
            
            fullscreenText.css({
                width  : width,
                height : height,
                top    : (this.windowHeight() - height) / 2,
                left   : (this.windowWidth() - width) / 2
            });
        }

    },
    
    wrapperWidth: function() {
        return Math.round(this.wrapper.width());
    },
    
    smallSize: function() {
        return Math.round(this.wrapperWidth() * 0.29296875);
    },
    
    bigSize: function() {
        return Math.round(this.wrapperWidth() * 0.614257812);
    },
    
    spacing: function() {
        return Math.round(this.wrapperWidth() * 0.028320312);
    },
    
    windowHeight: function() {
        return $(window).innerHeight();
    },
    
    windowWidth: function() {
        return $(window).innerWidth();
    },
    
    createNewHashTag: function() {
        var catHash = this.activeCategory;
        var boxHash = '';
        
        if(this.detailedBox !== undefined) {
            boxHash = boxHash + this.detailedBox.attr('id');
        }
        return window.location.hash = "/"+catHash+"/"+boxHash;
        
    },
    
    readHashTag: function(hash) {
        var array = hash.split('/').splice(1,2);
        var map = new HashMap();
        map.put('cat', array[0]);
        map.put('box', array[1]);
        return map;
    }
    
}

function isEmpty(o) {
  var o = {};
  for(var p in o) {
    if (o[p] != o.constructor.prototype[p])
      return false;
  }
  return true;
}

function HashMap() {}

HashMap.prototype.put = function(key, value) {
    this[key] = value;
};

HashMap.prototype.putVideo = function(key) {
    var videoInstance = _V_(key);
    this[key] = videoInstance;
};

HashMap.prototype.forEach = function(callback) {

    for(var prop in this) {
        if(this.hasOwnProperty(prop)) {
            callback(prop);
        }
    }

    return this;
};

HashMap.prototype.get = function(key) {
    if(typeof this[key] == 'undefined') {
        throw new ReferenceError("key is undefined");
    }
    return this[key];
};

HashMap.prototype.size = function() {
    var count = 0;

    for(var prop in this) {
        if(this.hasOwnProperty(prop)) {
            count++;
        }
    }

    return count;
};

var slider, sliderFullscreen, myScroll;

function initSwipe() {
    
    var bullets = $("#navigation li.dots");
    window.slider = new Swipe(document.getElementById('swipe-wrapper'), {
     callback: function(e, pos) {
         pinItLink(slider.slides[pos]);
         window.interactiveBoxes.pauseVideo(window.interactiveBoxes.detailedBox.videoDetail);
         $(bullets).removeClass("active");
         $(bullets).eq(pos).addClass("active");
     }
    });

    $("#navigation li.dots").bind("click", function(e) {
     e.preventDefault();
     slider.slide($("#navigation li.dots").index($(this)));
    });

    $("#navigation li.prev").bind("click", function(e) {
     e.preventDefault();
     slider.prev();
    });

    $("#navigation li.next").bind("click", function(e) {
     e.preventDefault();
     slider.next();
    });
}

function unbindSwipe() {
    $("#navigation li.dots").unbind("click");
    $("#navigation li.prev").unbind("click");
    $("#navigation li.next").unbind("click");
}

function initFullscreenSwipe() {
    var bulletsFullscreen = $("li.fullscreen-dots");
    var startPos = 0;
    if(window.slider !==undefined)
     startPos = slider.getPos();

    $(bulletsFullscreen[startPos]).addClass("active");

    sliderFullscreen = new Swipe(document.getElementById('swipe-wrapper-fullscreen'), {
     fullscreen : true,
     startSlide : startPos,
     callback: function(e, pos) {
         window.interactiveBoxes.pauseVideo(window.interactiveBoxes.detailedBox.videoDetailFullscreen);
         var i = bulletsFullscreen.length;
         while (i--) {
             $(bulletsFullscreen[i]).removeClass("active");
         }
         $(bulletsFullscreen[pos]).addClass("active");
     }
    });

    $("li.fullscreen-dots").bind("click", function(e) {
        e.preventDefault();
        slider.slide($("#fullscreen-wrapper li.fullscreen-dots").index($(this)));
        sliderFullscreen.slide($("#fullscreen-wrapper li.fullscreen-dots").index($(this)));
    });

    $("#fullscreen-prev").bind("click", function(e) {
        e.preventDefault();
        if(sliderFullscreen.getPos() === 0) {
            interactiveBoxes.loadPrevFullscreen();
        } else {
            slider.prev();
            sliderFullscreen.prev();
        }
    });

    $("#fullscreen-next").bind("click", function(e) {
        e.preventDefault();
        if(sliderFullscreen.getPos() === sliderFullscreen.length - 1) {
            interactiveBoxes.loadNextFullscreen();
        } else {
           slider.next();
           sliderFullscreen.next();
        }
    });
}

function pinItLink(slide) {
    var description = $(".box_item.detail").attr("data-name") + " " + ($(slide).attr('data-title') === undefined ? '' : $(slide).attr('data-title'));
    var url = $(slide).find('img').attr("src");
    refreshPinterestButton(window.location.href, url, description);
}

refreshPinterestButton = function (url, media, description) {
    var js, href, html, pinJs;
    url = escape(url);
    media = escape(media);
    description = escape(description);
    href = 'http://pinterest.com/pin/create/button/?url=' + url + '&media=' + media + '&description=' + description;
    src = "http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fdev.mass.com/#/View-All/1&amp;layout=button_count&amp;show_faces=true&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=80";
    $('div.pin-it a').attr("href", href);
    $('div.fb-button').html("<iframe src=\"http:\/\/www.facebook.com\/plugins\/like.php?href="+url+"&layout=button_count&show_faces=false&width=80&action=like&font=verdana&colorscheme=light\" scrolling=\"no\" frameborder=\"0\" style=\"border:none; overflow:hidden; width:80px; height:22px\" allowTransparency=\"true\"><\/iframe>");

    //add pinterest js
    js = document.createElement('script');
    js.src = "http://assets.pinterest.com/js/pinit.js";
    js.type = 'text/javascript';
    document.body.appendChild(js);
}
