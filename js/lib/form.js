$(function() {

	var ItemModel = Backbone.Model.extend({
		idAttribute: "ID",
		urlRoot: '/api/items',
		defaults: {
			ID_category : '',
			thumbnail : ''
		}
	});

	var SlideModel = Backbone.Model.extend({
		idAttribute: "ID",
		urlRoot: '/api/slides/',
		defaults: {
			type : 'image'
		}
	});

	var Slides = Backbone.Collection.extend({
		initialize: function(id) {
			this.item_id = id;
		},
		url: function() {
			return '/slides/item/id/'+this.item_id;
		},
		model: SlideModel
	});

	var FormView = Backbone.View.extend({
		initialize: function() {
			var self = this;
			_.bindAll(this, 'render', 'addSlide');
			this.slideList = [];
			this.slides = this.options.slides;
			this.rootUrl = this.options.rootUrl;

			this.slides.bind('reset', this.render);

			this.slides.bind('add', this.addSlide);

			// Init CK Editor
			CKEDITOR.replace( 'summary' );
			CKEDITOR.replace( 'text' );
			// Init Datepicker
			$("#datepicker").datepicker({dateFormat: 'yy-mm-dd'});
			// Init Sortable
			$( "#sortable" ).sortable({update : function() {
                var start = 1;
                $(this).children("li").each(function() {
                    self.slides.get(this.id).set({slide_order : start});
                    start++;
                });
			}});
		},
		events: {
			'click #add-category' : 'toggleCategories',
			'click .filefield' : 'uploadImage',
			'click a#addSlide' : 'createSlide',
			'click a#delete-all' : 'deleteSlides',
			'submit form' : 'saveModel'
		},
		render: function() {
			var self = this;
			this.slides.each(function(slide) {
				self.addSlide(slide);
			});
			return this;
		},
		toggleCategories: function(e) {
			e.preventDefault();
			$("#select-categories").slideToggle(600);
		},
		assignCategories: function(event, ui) {
			var categories = this.model.get('ID_category');
			var category_id = ui.item[0].id;
			if(categories.length < 1) {
				return this.model.set('ID_category', category_id);
			}
			var cats = this.model.get('ID_category').split(";");
			if(_.indexOf(cats, category_id ) < 0) {
				cats.push(category_id);
				this.model.set('ID_category', cats.join(";"));
			} else {
				alert("Category allready Assigned");
				$(ui.item[0]).remove();
			}
		},
		uploadImage: function(e) {
			e.preventDefault();
			var self = this;
			this.upload = new UploadPopup({
				name :'item',
				rootUrl: self.rootUrl,
				callback : function(data) {
					self.model.set('thumbnail', data);
					$(".project-image").attr('src',self.rootUrl + "/images/temp/" + data);
				}
			});
			this.upload.render();
		},
		addSlide: function(slide) {
			var slideView = new SlideView({model : slide, id : slide.cid, rootUrl : this.rootUrl });
			this.slideList.push(slideView);
			this.$el.find("#sortable").append(slideView.render().el);
		},
		createSlide: function(e) {
			e.preventDefault();
			this.slides.add(new SlideModel({ slide_order : this.$el.find("#sortable li").length + 1 }));
		},
		saveModel: function(e) {
			e.preventDefault();
			var self = this;
			var data = $.extend(this.model.attributes, $(e.currentTarget).serializeObject());
			this.savePopup = new SavePopup({ rootUrl : this.rootUrl});
			this.model.set(data);
			console.log(this.model.attributes);
			$.post(document.location.href, { 'model' : this.model.attributes }, function(data) {
				console.log(data);
				if(data.success) {
					var count = 0;
					self.savePopup.addText('Project Data Saved. Saving Slides...');
					if(self.slideList.length === 0 || self.slideList === undefined)
						return self.savePopup.addButton("Back To Projects", 'home');
					_.each(self.slideList, function(slideView) {
						slideView.saveSlide(data.model.ID, self.savePopup, function() {
							count++;
							if(count === self.slideList.length) {
								self.savePopup.addText('Project Saved Successfully !');
								self.savePopup.addButton("Back To Projects", 'home');
							}
						});
					});
				} else {
					self.savePopup.recordErrors(data.errors);
				}
			});
		},
		deleteSlides: function() {
			var confirmation = confirm("Are You Sure you want to delete all slides !");
			if(confirmation) {
				_.each(this.slideList, function(slideView) {
					slideView.deleteSlide();
				});
			} else
				return;
		}
	});

	var SlideView = Backbone.View.extend({
		tagName: 'li',
		className: 'item',
		id: this.id,
		initialize: function(){
			_.bindAll(this, 'render');
			this.model.bind('change', this.render);
			this.order = this.model.get('slide_order');
			this.rootUrl = this.options.rootUrl;
		},
		events: {
			'click a.delete-slide' : 'deleteConfirm',
			'change input[name$="name_slide"]' : 'changeName',
			'click a.toggle-type' : 'toggleType',
			'click a.view-image' : 'showImage',
			'click a.inputs' : 'uploadImage'
		},
		template: _.template($('#slide-template').html()),
		render: function() {
			this.$el.html(this.template( { slide : this.model.toJSON(), order : this.order } ));
			return this;
		},
		changeName: function(e){
			this.model.set('name', e.currentTarget.value);
		},
		toggleType: function() {
			var type = this.model.get('type') === "image" ? "video" : "image";
			this.model.set('type', type );
		},
		showImage: function(e) {
			e.preventDefault();
			var image;
			if(this.file) {
				image = this.rootUrl + "/images/temp/" + this.file;
			} else if(this.model.get('file')) {
				image = e.currentTarget.href + "/" + this.model.get('file');
			} else {
				alert("No Image Uploaded!");
				return;
			}
			var popup = new ImagePopup({ image : image });
		},
		uploadImage: function(e) {
			var el = e.currentTarget;
			var self= this;
			var field = el.getAttribute('data-image');
			this.upload = new UploadPopup({
				name :'slide',
				rootUrl: self.rootUrl,
				callback : function(data) {
					self[field] = data;
					self.model.set(field, data);
					console.log(self);
				}
			});
			this.upload.render();
		},
		saveSlide: function(id, savePopup, callback) {
			this.model.set('ID_item', id);
			$.post(document.location.href, { 'slide' : this.model.attributes }, function(data) {
				console.log(data);
				if(data.success) {
					savePopup.addText('Slide ' + data.slide.name + ' saved.');
					callback();
				} else {
					savePopup.recordErrors(data.errors);
				}
			});
		},
		deleteConfirm: function() {
			if(this.model.isNew())
				return this.removeView();

			var self = this;
			var confirmation = confirm("Are You Sure you want to delete this slide !");
			if(confirmation) {
				self.deleteSlide();
			} else
				return;
		},
		deleteSlide: function() {
			var self = this;
			this.model.destroy({success: function() {
				self.removeView();
			}, error: function(d, e) {
				console.log(d,e);
			}});
		},
		removeView: function() {
			this.undelegateEvents();
			this.remove();
		}
	});

	var ImagePopup = Backbone.View.extend({
		tagName: 'div',
		className: 'image-popup',
		id: 'image-popup',
		initialize: function() {
			var self = this;
			$("#loaderImage").fadeIn(150);
			$('body').append(this.el);
			this.$el.append(this.template());
			this.image = new Image();
			this.image.onload = function() {
				self.loadImage(self.image);
			};
			this.image.src = this.options.image;
		},
		template: _.template($("#popup-template").html()),
		events: {
			'click' : 'close'
		},
		loadImage: function(image) {
			var el = document.getElementById('popup-placeholder');
			if(image.width > window.innerWidth * 0.7 || image.height > window.innerHeight * 0.8) {

				var ratio = image.height * window.innerWidth * 0.7 / image.width < window.innerHeight * 0.8 ?
                    window.innerWidth * 0.7 / image.width :
					window.innerHeight * 0.8 / image.height;


                var width = ratio * image.width;
                var height = ratio * image.height;

                image.height = height;
                image.width = width;

            }
            $(image).fadeIn(300, function() {
				$(el).html(image);
				$(el).css({
					top  : (window.innerHeight - image.height) / 2 - 10,
					left : (window.innerWidth - image.width) / 2 - 10
				}).fadeIn(200, function() {
					$("#loaderImage").fadeOut(150);
				});
			});
		},
		close: function() {
			this.undelegateEvents();
			this.remove();
		}
	});

	var SavePopup = Backbone.View.extend({
		tagName: 'div',
		className: 'save-popup',
		id: 'save-popup',
		initialize: function() {
			$('body').append(this.el);
			this.$el.append(this.template());
			this.render();
		},
		template: _.template($("#save-popup-template").html()),
		events: {
			'click a#home' : 'homeClicked',
			'click a#close' : 'close'
		},
		render: function(){
			$('body').append(this.$el.fadeIn(400));
		},
		recordErrors: function(errors) {
			var self = this;
			_.each(errors, function(error){
				self.addText(error[0], true);
			});
			this.addButton('Fix Errors', 'close');
		},
		addText: function(text, error){
			$("#save-popup-body").append($("<p>", {text : text, 'class' : error ? 'red' : ''}));
		},
		addButton: function(text, id){
			$("#save-popup-buttons").html($("<a>", {text : text, href : "#", 'class' : 'button gray-bg rounded-corners small', id : id}));
		},
		homeClicked: function(){
			document.location.href = this.options.rootUrl + '/admin';
		},
		close: function(e) {
			e.preventDefault();
			this.undelegateEvents();
			this.remove();
		}
	});

	var UploadPopup = Backbone.View.extend({
		tagName: 'div',
		className: 'file-upload inputs-popup',
		id: 'file-upload',
		initialize: function() {
			this.name = this.options.name;
			this.callback = this.options.callback;
			this.mimeTypes = ['image/png', 'image/gif', 'image/jpeg'];
		},
		events: {
			'change input[type="file"]' : 'uploadImage',
			'click img.close-icon' : 'close'
		},
		template: _.template($("#upload-template").html()),
		render: function() {
			var self = this;
			$('body').append(this.$el.fadeIn(400));
			this.$el.append(this.template());
			self.dropContainer = document.getElementById("drop-area");
			self.dropContainer.addEventListener("dragenter", function(event){
				event.stopPropagation();event.preventDefault();
				$(self.dropContainer).addClass("active");
			}, false);
			self.dropContainer.addEventListener("dragover", function(event){
					event.stopPropagation();event.preventDefault();
			}, false);
			self.dropContainer.addEventListener("dragleave", function(event){
				event.stopPropagation();event.preventDefault();
				$(self.dropContainer).removeClass("active");
			}, false);
			self.dropContainer.addEventListener("drop", function(e) {
				self.drop(e, self);
			}, false);
		},
		uploadImage: function(e) {
			e.preventDefault();
			this.file = e.currentTarget.files[0];
			this.sendFile();
		},
		drop: function(e, view){
			e.preventDefault();
			e.stopPropagation();
			var data = e.dataTransfer;
			$(view.dropContainer).removeClass("active");
			view.file = data.files[0];
			view.sendFile();
		},
		sendFile: function() {
			var text;
			if(!_.contains(this.mimeTypes, this.file.type)) {
				text = 'Not supported type! <br /> Supported types : <br />';
				_.each(this.mimeTypes, function(type) {
					text = text + " " + type + "<br />";
				});
				this.$el.find("#output").html("<p class='red'>" + text + "</p>");
				return;
			}
			if(this.file.size > 8 * 1024 * 1024) {
				text = 'File is too big. <br /> Max size is 8 MB <br />';
				this.$el.find("#output").html("<p class='red'>" + text + "</p>");
				return;
			}
			uploadImage(this.file, this.name, this);
		},
		success: function(success, data) {
			if(!success) {
				this.$el.find("#output").html("<p class='red'>Image not uploaded !</p>");
			} else
			this.$el.find("#output").html("<p class='red'>Image Uploaded !</p>");
			this.previewImage(data);
			return this.callback(data);
		},
		progress: function(value) {
			this.$el.find("#output").html("<p>Uploading: " + value + "%</p>");
		},
		previewImage: function(src) {
			var image = new Image();
			var self = this;
			image.onload = function() {
				self.$el.find("#file-preview").html(image);
			};
			image.src = this.options.rootUrl + '/images/temp/' + src;
		},
		close: function() {
			this.undelegateEvents();
			this.remove();
		}
	});

	window.FormApp = function(opt) {
		var sync = Backbone.sync;
		var credentials = opt.credentials;
		this.id = opt.id;
		this.root = opt.root;
		Backbone.sync = function(method, model, options) {
			options.beforeSend = function(xhr) {
				xhr.setRequestHeader('X_REST_USERNAME', credentials.username);
				xhr.setRequestHeader('X_REST_PASSWORD', credentials.password);
			};
			sync(method, model, options);
		};
		
		this.fetch();
	};

	FormApp.prototype = {
		fetch: function() {
			var self = this;
			if(this.id) {
				this.item = new ItemModel({ID : this.id});
				this.item.fetch({
					success: function(data) {
						self.renderView();
					}
				});
			} else {
				this.item = new ItemModel();
				this.renderView();
			}
		},
		renderView: function(model) {
			var self = this;
			this.slides = new Slides(this.item.id);
			this.view = new FormView({
				model : this.item,
				rootUrl : self.root,
				slides : this.slides,
				el: document.getElementById('wrapper')
			});
			this.slides.fetch();
		}
	};

	function uploadImage(file, name, callback) {
		var self = this;
		var xhr = new XMLHttpRequest();
		var fd = new FormData();
		this.xhr = xhr;
		this.xhr.upload.addEventListener("progress", function(e){
			if(e.lengthComputable) {
				var percentage = Math.round((e.loaded * 100) / e.total);
				callback.progress(percentage);
			}
		}, false);
		xhr.open("POST", '/Admin/default/uploadFile', true);
		xhr.overrideMimeType('text/plain; charset=x-user-defined-binary');
		xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
				var json = JSON.parse(xhr.response);
				callback.success(json.success, json.filename);
			}
		};
		fd.append('file', file);
		fd.append('name', name);
		xhr.send(fd);
	}

	$.fn.serializeObject = function(){
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

});