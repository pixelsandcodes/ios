# Mutant Mind project

## Usage:
* Download the project

* Place it in your root folder

* import the `_DB_.sql` file in phpmyadmin

* change the `main.php` file located in `ROOT-FOLDER/protected/config/` here:

      	'db'=>array(
  			'connectionString' => 'mysql:host=DB_HOST;dbname=ios',
  			'emulatePrepare' => true,
  			'username' => 'USERNAME',
  			'password' => 'PASSWORD',
  			'charset' => 'utf8',
  		),